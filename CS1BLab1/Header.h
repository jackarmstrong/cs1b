/*************************************************************************
*  PROGRAMMED BY : Jack Armstrong and Noah Connors
*  CLASS         : CS1B
*  SECTION       : MWF: 08:00 - 10:50
*  LAB #1		 : CS1A Review
**************************************************************************/


#ifndef HEADER_H_
#define HEADER_H_

#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

/*************************************************************************
 * InputKid
 * -----------------------------------------------------------------------
 * This function inputs a kid and returns the first name, age,
 * whether the kid is a vegetarian, and whether the kid likes cheese.
 ************************************************************************/
void InputKid(short   kidNumber    // IN  - The kid number in the group
		    , string &firstName    // OUT - The first name of the kid
			, short  &age          // OUT - The age of the kid
			, bool   &vegetarian   // OUT - Whether the kid is vegetarian
			, bool   &likeCheese); // OUT - Whether the kid likes cheese

/*************************************************************************
 * PrintAttractions
 * -----------------------------------------------------------------------
 * This function will take in the kid preferences and output the attraction
 * for the kid passed in.
 * It also increments the price variable passed in.
 ************************************************************************/
void PrintAttractions(string name       // IN  - The kid's name
		            , short  age        // IN  - The kid's age
					, bool   vegetarian // IN  - Whether the kid is vegetarian
					, bool   likeCheese // IN  - Whether the kid likes cheese
					, float &price);    // OUT - The price for the kid

#endif /* HEADER_H_ */

/*************************************************************************
*  PROGRAMMED BY : Jack Armstrong and Noah Connors
*  CLASS         : CS1B
*  SECTION       : MWF: 08:00 - 10:50
*  LAB #1		 : CS1A Review
**************************************************************************/

#include "Header.h"

/**************************************************************************
 * This program accepts 10 kids and prints the theme park attractions and
 * meals.  It also prints the price for the whole trip and average price
 * per kid.
 *
 * INPUTS:
 * OUTPUTS:
 *************************************************************************/

int main()
{
	/*********************************************************************
	 * CONSTANTS
	 * -------------------------------------------------------------------
	 * CLASS HEADER CONSTANTS
	 * -------------------------------------------------------------------
	 * PROGRAMMER	: The programmers of this program
	 * CLASS        : The class of the programmers
	 * SECTION      : The class section
	 * LAB_NUM      : The lab number
	 * LAB_NAME		: The lab name
	 * -------------------------------------------------------------------
	 * PROGRAM CONSTANTS
	 * -------------------------------------------------------------------
	 * NUM_OF_KIDS	: The number of kids to input
	 * COL_SIZE		: The column size for outputting
	 *********************************************************************/
	const char PROGRAMMER[32] = "Jack Armstrong and Noah Connors";
	const char CLASS[5]		  = "CS1B";
	const char SECTION[25]    = "MWF: 08:00 - 10:50";
	const int  LAB_NUM		  = 1;
	const char LAB_NAME[40]   = "CS1A Review";

	const int  NUM_OF_KIDS    = 10;
	const int  COL_SIZE       = 33;

	cout << left;
	cout << "***************************************************";
	cout << "\n*  PROGRAMMED BY : " << PROGRAMMER;
	cout << "\n*  " << setw(14) << "CLASS"  << ": " << CLASS;
	cout << "\n*  " << setw(14) << "SECTION"  << ": " << SECTION;
	cout << "\n*  LAB #" << setw(7) << LAB_NUM << ": " << LAB_NAME;
	cout << "\n***************************************************\n\n";
	cout << right;

	float  price;      //      CALC & OUT - The total price so far
	short  kidNumber;  //             OUT - The current kid number
	string name;	   // IN &        OUT - The kid's name
	short  age;        // IN & CALC       - The kid's age
	bool   vegetarian; // IN & CALC       - Whether the kid is vegetarian
	bool   likeCheese; // IN & CALC       - Whether the kid likes cheese

	price = 0;
	// IN & CALC - Input the 10 kids and output the specific attractions
	//             for every individual kid
	for(kidNumber = 1; kidNumber <= NUM_OF_KIDS; kidNumber++) {
		InputKid(kidNumber, name, age, vegetarian, likeCheese);
		cout << endl;
		PrintAttractions(name, age, vegetarian, likeCheese, price);
		cout << "\n\n";
	} // END FOR(kidNumber)

	// OUTPUT - Print the total price and average cost per kid
	cout << left << setprecision(2) << fixed << "\n\n\n";
	cout << setw(COL_SIZE) << "The total cost for the day is:" << "$"
		 << right << setw(7) << price << endl;
	cout << left << setw(COL_SIZE) << "The average cost per kid is:" << "$"
		 << right << setw(7) << price / NUM_OF_KIDS << endl;

	return 0;
} // end main

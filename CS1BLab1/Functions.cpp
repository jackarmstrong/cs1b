/*************************************************************************
*  PROGRAMMED BY : Jack Armstrong and Noah Connors
*  CLASS         : CS1B
*  SECTION       : MWF: 08:00 - 10:50
*  LAB #1		 : CS1A Review
**************************************************************************/

#include "Header.h"

/*************************************************************************
 * FUNCTION InputKid
 * -----------------------------------------------------------------------
 * This function inputs a kid and returns the first name, age,
 * whether the kid is a vegetarian, and whether the kid likes cheese.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS :
 * 		- kidNumber: the kid number of the group of kids
 * POST-CONDITIONS:
 * 		- firstName  : The first name of the kid
 * 		- age        : The kid's age
 * 		- vegetarian : Whether the kid is vegetarian
 * 		- likeCheese : Whether the kid likes cheese
 ************************************************************************/
void InputKid(short   kidNumber   // IN  - The kid number in the group
		    , string &firstName   // OUT - The first name of the kid
			, short  &age         // OUT - The age of the kid
			, bool   &vegetarian  // OUT - Whether the kid is vegetarian
			, bool   &likeCheese) // OUT - Whether the kid likes cheese
{
	/*********************************************************************
	 * CONSTANTS
	 * -------------------------------------------------------------------
	 * COL_SIZE		: The column size for the prompts
	 ********************************************************************/
	const int COL_SIZE = 33;

	char vegetarianChar; // IN & CALC - Whether the kid is vegetarian
	char likeCheeseChar; // IN & CALC - Whether the kid likes cheese

	// INPUT - Input the kid preferences
	cout << left;
	cout << "Kid #" << kidNumber << ":\n";

	cout << setw(COL_SIZE) << "What is your kid's name?";
	cin  >> firstName;
	cin.ignore(1000, '\n');

	cout << setw(COL_SIZE) << "How old is the kid?";
	cin  >> age;
	cin.ignore(1000, '\n');

	cout << setw(COL_SIZE) << "Vegetarian (Y/N)?";
	cin.get(vegetarianChar);
	cin.ignore(1000, '\n');

	cout << setw(COL_SIZE) << "Does he/she like cheese (Y/N)?";
	cin.get(likeCheeseChar);
	cin.ignore(1000, '\n');

	cout << right;
	vegetarian = vegetarianChar == 'Y';
	likeCheese = likeCheeseChar == 'Y';
}
/*************************************************************************
 * FUNCTION PrintAttractions
 * -----------------------------------------------------------------------
 * This function will take in the kid preferences and output the attraction
 * for the kid passed in.
 * It also increments the price variable passed in.
 * -----------------------------------------------------------------------
 * PRE-CONDTIIONS :
 * 		- name       : The kid's name
 * 		- age        : The kid's age
 * 		- vegetarian : Whether the kid is vegetarian
 * 		- likeCheese : Whether the kid likes cheese
 * POST-CONDITIONS:
 * 		- price      : The price for the kid
 ************************************************************************/
void PrintAttractions(string name       // IN  - The kid's name
		            , short  age        // IN  - The kid's age
					, bool   vegetarian // IN  - Whether the kid is vegetarian
					, bool   likeCheese // IN  - Whether the kid likes cheese
					, float &price)     // OUT - The price for the kid
{
	cout << name << " will be going on the ";
	// PROCESSING - Calculate the attractions and entry fee
	if(age < 5)
	{
		cout << "Ferris Wheel and will be visiting the Sheep Petting Zoo\n";
	} // END IF(age < 5)
	else if(age <= 12)
	{
		cout << "Teacups and will be playing Laser Tag\n";
		price += 15;
	} // END IF(age <= 12)
	else
	{
		cout << "Roller Coaster and the Zip Line\n";
		price += 20;
	} // END ELSE

	cout << "Pack a ";
	// PROCESSING - Calculate the meal and meal fee
	if(vegetarian)
	{
		if(likeCheese)
		{
			cout << "Cheese Pizza";
			price += 2.5;
		} // END IF(likeCheese)
		else
		{
			cout << "Happy Garden meal";
			price += 1.75;
		} // END ELSE
	} // END IF(vegetarian)
	else
	{
		if(likeCheese)
		{
			cout << "Cheeseburger";
			price += 3.5;
		} // END IF(likeCheese)
		else
		{
			cout << "Hamburger";
			price += 3.25;
		} // END ELSE
	} // END ELSE
	cout << " for " << name << "!\n";
}

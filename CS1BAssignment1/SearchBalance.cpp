/*************************************************************************
 * AUTHOR       : Jack Armstrong
 * STUDENT ID   : 1017947
 * ASSIGNMENT #1: Functions And Arrays
 * CLASS        : CS1B
 * SECTION      : M/W 8:00am
 * DUE DATE     : 2/10
 ************************************************************************/

#include "Assignment1.h"

/*************************************************************************
 * FUNCTION SearchBalance
 * -----------------------------------------------------------------------
 * This function will take in a boolean, int, and float array to search.
 * It will search for the smallest or largest in the array given to it.
 * If the first boolean is true, it will search for the smallest.
 * Otherwise the function will search for the largest.  The function
 * returns the index of the smallest/largest. If all the values in the
 * array are equal, this function returns -1
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 			- searchForSmallest: Whether to search for the smallest or
 * 								 the biggest
 * 			- AR_SIZE          : The array size
 * 			- ar[]			   : The array to search
 * POST-CONDITIONS:
 * This function returns the index of the smallest/largest number.
 ************************************************************************/
int SearchBalance(      bool searchForSmallest // IN - Whether to search
											   //      for the smallest
				, const int   AR_SIZE          // IN - The array size
				,       float ar[])			   // IN - The array
{
	int count;                // CALC       - The for loop index
	int largestSmallestIndex; // CALC & OUT - The largest/smallest index

	// initialize to starting value
	largestSmallestIndex = 0;

	// search for the smallest/biggest
	for(count = 0; count < AR_SIZE; count++)
	{
		// check if we want to search for the smallest or biggest
		if(searchForSmallest)
		{
			if(ar[largestSmallestIndex] > ar[count])
			{
				largestSmallestIndex = count;
			} // END IF
		} // END IF
		else
		{
			if(ar[largestSmallestIndex] < ar[count])
			{
				largestSmallestIndex = count;
			} // END IF
		} // END ELSE
	} // END FOR(count)
	return largestSmallestIndex;
}

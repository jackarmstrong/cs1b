/*************************************************************************
 * AUTHOR       : Jack Armstrong
 * STUDENT ID   : 1017947
 * ASSIGNMENT #1: Functions And Arrays
 * CLASS        : CS1B
 * SECTION      : M/W 8:00am
 * DUE DATE     : 2/10
 ************************************************************************/

#include "Assignment1.h"

/*************************************************************************
 * FUNCTION FindSumAvg
 * -----------------------------------------------------------------------
 * This function will take in a boolean (whether to find the sum or avg),
 * integer for the array size, and the actual float array and returns the
 * sum/average for the array as specified by the first parameter.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 			- findSum : Whether to find the sum or average
 * 			- AR_SIZE : The size of the array to operate on
 * 			- ar[]    : The array to find the sum/average of
 * POST-CONDITIONS:
 * This function returns the sum/average of the specified array.
 ************************************************************************/
float FindSumAvg(      bool  findSum // IN - Whether to find the sum/avg
		       , const int   AR_SIZE // IN - The array size
		       ,       float ar[])   // IN - The array
{
	float sum;   // CALC - The sum of the array
	int   count; // CALC - The for loop counter


	// initialize sum to starting value
	sum = 0;

	// PROCESSING - Find the sum of the array
	for(count = 0; count < AR_SIZE; count++)
	{
		sum+=ar[count];
	} // END FOR(count)

	// check if we need to return the sum or average
	if(findSum)
	{
		return sum;
	} // END IF(findSum)
	else
	{
		return sum / AR_SIZE;
	} // END ELSE
}

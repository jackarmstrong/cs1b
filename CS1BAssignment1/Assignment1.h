/*************************************************************************
 * AUTHOR       : Jack Armstrong
 * STUDENT ID   : 1017947
 * ASSIGNMENT #1: Functions And Arrays
 * CLASS        : CS1B
 * SECTION      : M/W 8:00am
 * DUE DATE     : 2/10
 ************************************************************************/


#ifndef ASSIGNMENT1_H_
#define ASSIGNMENT1_H_

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
using namespace std;

/*************************************************************************
 * MenuOptions
 * -----------------------------------------------------------------------
 * This enumerated type represents the menu options for this program.
 * They are arranged so their numerical values are the same as their
 * equivelents as menu options.
 ************************************************************************/
enum MenuOptions
{
	EXIT,
	FIND_LARGER_BALANCE,
	FIND_SMALLER_BALANCE,
	OBTAIN_SUM_OF_BALANCE,
	OBTAIN_AVG_OF_BALANCE,
	FIND_PERSON
};

/*************************************************************************
 * FindSumAvg
 * -----------------------------------------------------------------------
 * This function will take in a boolean (whether to find the sum or avg),
 * integer for the array size, and the actual float array and returns the
 * sum/average for the array as specified by the first parameter.
 ************************************************************************/
float FindSumAvg(      bool  findSum // IN - Whether to find the sum/avg
		       , const int   AR_SIZE // IN - The array size
		       ,       float ar[]);  // IN - The array
/*************************************************************************
 * InputPeople
 * -----------------------------------------------------------------------
 * This function takes in the input file name, the array's size,
 * the names array, the ids array, and the balances array. It then reads
 * from the specified file into the 3 arrays like this:
 * NAME
 * ID BALANCE
 ************************************************************************/
void InputPeople(      string inFileName   // IN  - The input file name
		       , const int    AR_SIZE      // IN  - The array size
		       ,       string names   []   // OUT - The names array
		       ,       int    ids     []   // OUT - The ids array
		       ,       float  balances[]); // OUT - The balances array
/*************************************************************************
 * PrintHeader
 * -----------------------------------------------------------------------
 * This function prints the class header to the specified output stream.
 ************************************************************************/
void PrintHeader(ostream& out      // IN - The output stream to print to
		       , string programmer // IN - The programmer's name
		       , string className  // IN - The class name
		       , string section    // IN - The class section
		       , string assName    // IN - The assignment name
		       , int assNum);      // IN - The assignment type
/*************************************************************************
 * SearchStringAr
 * -----------------------------------------------------------------------
 * This function takes in an array size, string array and item to search
 * for and returns the index of the search item passed in inside the
 * given array.  If the element is not found, this function returns -1.
 ************************************************************************/
int SearchStringAr(const int    AR_SIZE		 // IN - The array size
				 ,       string ar[]		 // IN - The array to search in
				 ,       string searchItem); // IN - The item to search for
/*************************************************************************
 * SearchBalance
 * -----------------------------------------------------------------------
 * This function will take in a boolean, int, and float array to search.
 * It will search for the smallest or largest in the array given to it.
 * If the first boolean is true, it will search for the smallest.
 * Otherwise the function will search for the largest.  The function
 * returns the index of the smallest/largest. If all the values in the
 * array are equal, this function returns -1
 ************************************************************************/
int SearchBalance(      bool searchForSmallest // IN - Whether to search
											   //      for the smallest
				, const int   AR_SIZE          // IN - The array size
				,       float ar[]);		   // IN - The array


#endif /* ASSIGNMENT1_H_ */

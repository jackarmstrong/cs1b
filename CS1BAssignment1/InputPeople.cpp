
/*************************************************************************
 * AUTHOR       : Jack Armstrong
 * STUDENT ID   : 1017947
 * ASSIGNMENT #1: Functions And Arrays
 * CLASS        : CS1B
 * SECTION      : M/W 8:00am
 * DUE DATE     : 2/10
 ************************************************************************/

#include "Assignment1.h"

/*************************************************************************
 * FUNCTION InputPeople
 * -----------------------------------------------------------------------
 * This function takes in the input file name, the array's size,
 * the names array, the ids array, and the balances array. It then reads
 * from the specified file into the 3 arrays like this:
 * NAME
 * ID BALANCE
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 		- inFileName : The name of the input file
 * POST-CONDITIONS:
 * 		- names[]    : The names array to input
 * 		- ids[]      : The ids array to input
 * 		- balances[] : The balances array to input
 ************************************************************************/
void InputPeople(      string inFileName   // IN  - The input file name
		       , const int    AR_SIZE      // IN  - The array size
		       ,       string names   []   // OUT - The names array
		       ,       int    ids     []   // OUT - The ids array
		       ,       float  balances[])  // OUT - The balances array
{
	ifstream fin; // IN & CALC - The file stream input variable
	int count;    //      CALC - The for loop counter

	fin.open(inFileName.c_str());

	count = 0;
	// input every name/id/balance until there is no more array or input
	// file left
	while(count < AR_SIZE && fin)
	{
		getline(fin, names[count]);
		fin >> ids[count];
		fin >> balances[count];
		fin.ignore(1000, '\n');
		count++;
	} // END WHILE
}

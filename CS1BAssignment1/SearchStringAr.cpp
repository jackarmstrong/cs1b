
/*************************************************************************
 * AUTHOR       : Jack Armstrong
 * STUDENT ID   : 1017947
 * ASSIGNMENT #1: Functions And Arrays
 * CLASS        : CS1B
 * SECTION      : M/W 8:00am
 * DUE DATE     : 2/10
 ************************************************************************/

#include "Assignment1.h"

/*************************************************************************
 * FUNCTION SearchStringAr
 * -----------------------------------------------------------------------
 * This function takes in an array size, string array and item to search
 * for and returns the index of the search item passed in inside the
 * given array.  If the element is not found, this function returns -1.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 			- AR_SIZE		: The array size
 * 			- ar[]			: The array to search in
 * 			- searchItem	: The item to search for
 * POST-CONDITIONS:
 * This function returns the index of searchItem, or if it is not found -1.
 ************************************************************************/
int SearchStringAr(const int    AR_SIZE		// IN - The array size
				 ,       string ar[]		// IN - The array to search in
				 ,       string searchItem) // IN - The item to search for
{
	int index;	// CALC - The index of the searchItem
	bool found; // CALC - Whether the item is found or not

	index = 0;
	found = false;

	while(index < AR_SIZE && !found)
	{
		found = (ar[index] == searchItem);
		if(!found)
		{
			index++;
		}
	}

	return (found ? index : -1);
}

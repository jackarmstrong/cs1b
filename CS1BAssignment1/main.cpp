/*************************************************************************
 * AUTHOR       : Jack Armstrong
 * STUDENT ID   : 1017947
 * ASSIGNMENT #1: Functions And Arrays
 * CLASS        : CS1B
 * SECTION      : M/W 8:00am
 * DUE DATE     : 2/10
 ************************************************************************/

#include "Assignment1.h"

// Documentation that goes here will be discussed later
int main()
{
	/*********************************************************************
	 * CONSTANTS
	 * -------------------------------------------------------------------
	 * USED FOR CLASS HEADING – ALL WILL BE OUTPUT
	 * -------------------------------------------------------------------
	 * PROGRAMMER 		: Programmer's Name
	 * CLASS 			: Student's Course
	 * SECTION 			: Class Days and Times
	 * LAB_NUM 			: Lab Number (specific to this lab)
	 * LAB_NAME		 	: Title of the Lab
	 * -------------------------------------------------------------------
	 * PROGRAM CONSTANTS
	 * -------------------------------------------------------------------
	 * NUM_OF_PEOPLE	: The number of people to input from the file
	 * MENU_OPTIONS		: the options for actions when running the program
	 ********************************************************************/

	const string PROGRAMMER     = "Jack Armstrong";
	const string CLASS          = "CS1B";
	const string SECTION        = "M/W 8:00am";
	const int    ASS_NUM        = 1;
	const string ASS_NAME       = "Functions And Arrays";

	const int    NUM_OF_PEOPLE  = 10;

	const string MENU_OPTIONS   = "\nMENU OPTIONS\n"
								  "\n1 – Find the larger balance"
								  "\n2 – Find the smaller balance"
								  "\n3 – Obtain the sum of all balances"
								  "\n4 – Obtain the average of all balances"
								  "\n5 – Find person"
								  "\n0 - Exit"
								  "\nEnter an option (0 to exit): ";

	ofstream fout;						//			   OUT - The output file
	string inFileName;					// IN & CALC	   - The input filename
	string outFileName;					// 	    CALC & OUT - The output filename

	string namesAr  [NUM_OF_PEOPLE];	// IN & CALC & OUT - The names array
	int    idsAr    [NUM_OF_PEOPLE];	// IN & CALC & OUT - The ids array
	float  balanceAr[NUM_OF_PEOPLE];	// IN & CALC & OUT - The array
										//					 of balances

	MenuOptions userOption;				// IN & CALC	   - The user's option
	int         userOptionAsInt;		// IN & CALC	   - The user's option
	bool        keepGoing;				// 		CALC	   - Whether to keep going

	int indexFound;						//      CALC & OUT - The index found
	float sumOrAvg;						//		CALC & OUT - The sum/avg

	int id;								// IN & CALC & OUT - The id
	float balance;						// IN & CALC & OUT - The balance
	string name;						// IN & 	   OUT - The user's name
	string searchItem;					// IN & CALC & OUT - The item to search

	// OUTPUT - Print the class header
	PrintHeader(cout, PROGRAMMER, CLASS, SECTION, ASS_NAME, ASS_NUM);

	/*********************************************************************
	 * INPUT
	 * -------------------------------------------------------------------
	 * inFileName	: The input file name
	 * outFileName	: The output file name
	 * namesAr		: The names array to input from the file
	 * idsAr		: The ids array to input from the file
	 * balanceAr	: The balances array to input from the file
	 ********************************************************************/
	cout << left;
	cout << setw(40) << "What input file would you like to use?";
	getline(cin, inFileName);

	cout << setw(40) << "What output file would you like to use?";
	getline(cin, outFileName);
	cout << right;
	fout.open(outFileName.c_str());

	PrintHeader(fout, PROGRAMMER, CLASS, SECTION, ASS_NAME, ASS_NUM);
	InputPeople(inFileName, NUM_OF_PEOPLE, namesAr, idsAr, balanceAr);

	// INPUT - Input the user's option and figure out if it means EXIT
	cout << MENU_OPTIONS;
	cin  >> userOptionAsInt;
	cin.ignore(1000, '\n');

	if(userOptionAsInt > 5)
	{
		userOption = EXIT;
	} // END IF
	else
	{
		userOption = (MenuOptions) userOptionAsInt;
	} // END ELSE

	searchItem = "";
	keepGoing = (userOption != EXIT && searchItem != "done");

	fout << setprecision(2) << fixed;

	// PROCESSING - Ask the user for an option and output the result to
	//			  - the output file
	while(keepGoing)
	{
		// SWITCH - Switch on the user's option
		switch(userOption)
		{
		case FIND_LARGER_BALANCE:   indexFound = SearchBalance(false, NUM_OF_PEOPLE, balanceAr);
									cout << "\nFinding the larger balance...\n";

									name = namesAr[indexFound];
									id   = idsAr[indexFound];
									balance = balanceAr[indexFound];

									fout << "Larger Balance:\n";
									fout << left << setw(9) << "ID #" << setw(25) << "NAME" << "BALANCE DUE" << endl;
									fout <<         setw(9) << "----" << setw(25) << "--------------------" << "-----------" << endl;
									fout <<         setw(9) << id     << setw(25) << name   << "$" << right << setw(10) << balance << endl << endl;
									break;
		case FIND_SMALLER_BALANCE:  indexFound = SearchBalance(true, NUM_OF_PEOPLE, balanceAr);
								    cout << "\nFinding the smaller balance...\n";

								    name = namesAr[indexFound];
								    id   = idsAr[indexFound];
								    balance = balanceAr[indexFound];

								    fout << "Smaller Balance:\n";
								    fout << setprecision(2) << fixed;
								    fout << left << setw(9) << "ID #" << setw(25) << "NAME" << "BALANCE DUE" << endl;
								    fout <<         setw(9) << "----" << setw(25) << "--------------------" << "-----------" << endl;
								    fout <<         setw(9) << id     << setw(25) << name   << "$" << right << setw(10) << balance << endl << endl;
									break;
		case OBTAIN_SUM_OF_BALANCE: sumOrAvg = FindSumAvg(true, NUM_OF_PEOPLE, balanceAr);

									cout << "\nObtaining the sum of all balances...\n";

									fout << "Sum of Balance for all persons:\n";
									fout << "$" << setw(10) << sumOrAvg << endl << endl;
									break;
		case OBTAIN_AVG_OF_BALANCE: sumOrAvg = FindSumAvg(false, NUM_OF_PEOPLE, balanceAr);

									cout << "\nObtaining the average of all balances...\n";

									fout << "Average Balance for all persons:\n";
									fout << "$" << setw(10) << sumOrAvg << endl << endl;
									break;
		case FIND_PERSON:			cout << "\nWho do you want to search for (enter done to exit): ";
									getline(cin, searchItem);

									if(searchItem != "done")
									{
										indexFound = SearchStringAr(NUM_OF_PEOPLE, namesAr, searchItem);
										if(indexFound == -1)
										{
											cout << searchItem << " was not found.\n";
										} // END IF
										else
										{
											cout << "Found.\n";
											name = namesAr[indexFound];
											id = idsAr[indexFound];
											balance = balanceAr[indexFound];

											fout << "Search Name:\n";
											fout << left << setw(9) << "ID #" << setw(25) << "NAME" << "BALANCE DUE" << endl;
										    fout <<         setw(9) << "----" << setw(25) << "--------------------" << "-----------" << endl;
										    fout <<         setw(9) << id     << setw(25) << name   << "$" << right << setw(10) << balance << endl << endl;
										} // END ELSE
									} // END IF
									break;
		} // END SWITCH
		cout << MENU_OPTIONS;
		cin  >> userOptionAsInt;
		cin.ignore(1000, '\n');

		if(userOptionAsInt > 5)
		{
			userOption = EXIT;
		} // END IF
		else
		{
			userOption = (MenuOptions) userOptionAsInt;
		} // END ELSE
		keepGoing = (userOption != EXIT && searchItem != "done");

	} // END WHILE

	// Close the output file
	fout.close();

	cout << "\nThank you for using my program.\n";

	return 0;
}

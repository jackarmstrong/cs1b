
#include "Header.h"

Days StringToDay(string dayAsStr)
{
	Days day;
	switch(dayAsStr[0])
	{
	case 'M': day = MONDAY;
			  break;
	case 'T': if(dayAsStr[1] == 'u')
			  {
			  	  day = TUESDAY;
			  }
			  else
			  {
				  day = THURSDAY;
			  }
			  break;
	case 'W': day = WEDNESDAY;
			  break;
	case 'F': day = FRIDAY;
			  break;
	case 'S': if(dayAsStr[1] == 'a')
			  {
			  	  day = SATURDAY;
			  }
			  else
			  {
				  day = SUNDAY;
			  }
			  break;
	default : day = INVALID_DAY;
			  break;
	}
	return day;
}

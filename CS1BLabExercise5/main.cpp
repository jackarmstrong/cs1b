
#include "Header.h"

int main()
{
	Days theDay;
	string userInput;
	string dayAsStr;

	cout << "What day is today (enter \"done\" to exit) ? ";
	getline(cin, userInput);

	while(userInput != "done")
	{
		theDay = StringToDay(userInput);
		if(theDay != INVALID_DAY)
		{
			dayAsStr = DayToString(theDay);
			cout << "Today is " << dayAsStr << "!";
		}
		else
		{
			cout << "****  Invalid day input!  ****";
		}
		cout << endl << endl;

		cout << "What day is today (enter \"done\" to exit) ? ";
		getline(cin, userInput);
	}
	cout << "\nThank you for using my Days calculator.\n";
	return 0;
}

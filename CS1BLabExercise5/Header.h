/*
 * Header.h
 *
 *  Created on: Feb 22, 2016
 *      Author: jackarmstrong
 */

#ifndef HEADER_H_
#define HEADER_H_

#include <iostream>
#include <string>
using namespace std;

enum Days
{
	SUNDAY,
	MONDAY,
	TUESDAY,
	WEDNESDAY,
	THURSDAY,
	FRIDAY,
	SATURDAY,
	INVALID_DAY
};
const int NUM_OF_DAYS = 7;
const string DAYS_AS_STRINGS[NUM_OF_DAYS] =
{
	"SUNDAY",
	"MONDAY",
	"TUESDAY",
	"WEDNESDAY",
	"THURSDAY",
	"FRIDAY",
	"SATURDAY"
};

Days StringToDay(string dayAsStr);
string DayToString(Days myDay);

#endif /* HEADER_H_ */

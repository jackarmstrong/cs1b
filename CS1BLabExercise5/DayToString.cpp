
#include "Header.h"

string DayToString(Days myDay)
{
	string asStr;
	if(myDay == INVALID_DAY)
	{
		asStr = "Invalid day";
	}
	else
	{
		asStr = DAYS_AS_STRINGS[myDay];
	}
	return asStr;
}

/*************************************************************************
 * AUTHOR       : Jack Armstrong & Austin Ayres
 * STUDENT ID   : 1017947        & 1012547
 * LAB #4       : Introduction to Arrays
 * CLASS        : CS1B
 * SECTION      : M/W/F 8:00am
 * DUE DATE     : 2/26
 ************************************************************************/

#include "Header.h"

/*************************************************************************
 * FUNCTION InstancesOfStringInAr
 * -----------------------------------------------------------------------
 * This function takes in an array size, a string array, and a searchItem
 * and returns the # of instances of searchItem in the array.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 				- AR_SIZE		: The size of the array
 * 				- ar[]			: The array to search in
 * 				- searchItem	: The item to search for
 * POST-CONDITIONS:
 * This function returns the number of instances of searchItem in the
 * string arrray.
 ************************************************************************/
int InstancesOfStringInAr(const int AR_SIZE	  // IN - The size of the array
						, string ar[]		  // IN - The array to search in
						, string searchItem)  // IN - The item to search for
{
	int instances;	// CALC & OUT - The number of instances of searchItem
	int count;		// CALC		  - The current item index

	// Start instances at 0
	instances = 0;

	// Loop through the array
	for(count = 0; count < AR_SIZE; count++)
	{
		// Check if the current item is an instance of searchItem
		if(ar[count] == searchItem)
		{
			instances++;
		} // END IF
	} // END FOR

	return instances;
}


/*************************************************************************
 * AUTHOR       : Jack Armstrong & Austin Ayres
 * STUDENT ID   : 1017947        & 1012547
 * LAB #4       : Introduction to Arrays
 * CLASS        : CS1B
 * SECTION      : M/W/F 8:00am
 * DUE DATE     : 2/26
 ************************************************************************/

#ifndef HEADER_H_
#define HEADER_H_


#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
using namespace std;

/*************************************************************************
 * FUNCTION PrintHeader
 * -----------------------------------------------------------------------
 * This function prints the class header to the specified output stream.
 ************************************************************************/
void PrintHeader(ostream& out      // IN - The output stream to print to
		       , string programmer // IN - The programmer's name
		       , string className  // IN - The class name
		       , string section    // IN - The class section
		       , string assName    // IN - The assignment name
		       , int assNum);      // IN - The assignment type

/*************************************************************************
 * FUNCTION InstancesOfStringInAr
 * -----------------------------------------------------------------------
 * This function takes in an array size, a string array, and a searchItem
 * and returns the # of instances of searchItem in the array.
 * -----------------------------------------------------------------------
 ************************************************************************/
int InstancesOfStringInAr(const int AR_SIZE	  // IN - The size of the array
						, string ar[]		  // IN - The array to search in
						, string searchItem); // IN - The item to search for


#endif /* HEADER_H_ */

/*************************************************************************
 * AUTHOR       : Jack Armstrong & Austin Ayres
 * STUDENT ID   : 1017947        & 1012547
 * LAB #4       : Introduction to Arrays
 * CLASS        : CS1B
 * SECTION      : M/W/F 8:00am
 * DUE DATE     : 2/26
 ************************************************************************/

#include "Header.h"

/*************************************************************************
 * FUNCTION PrintHeader
 * -----------------------------------------------------------------------
 * This function prints the class header to the specified output stream.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 			- out		 : The output file
 * 			- programmer : The programmer's name
 * 			- className  : The class name
 * 			- section    : The class section
 * 			- assName    : The assignment name
 * 			- assNum     : The assignment number
 * POST-CONDITIONS:
 * This function outputs the class header to out.
 ************************************************************************/
void PrintHeader(ostream& out      // IN - The output stream to print to
		       , string programmer // IN - The programmer's name
		       , string className  // IN - The class name
		       , string section    // IN - The class section
		       , string assName    // IN - The assignment name
		       , int assNum)       // IN - The assignment type
{
	// OUTPUT Class Heading
	out << left;
	out << "**************************************************";
	out << "\n* PROGRAMMED BY : " << programmer;
	out << "\n* " << setw(14) << "CLASS" << ": " << className;
	out << "\n* " << setw(14) << "SECTION" << ": " << section;
	out << "\n* Lab #" << setw(9) << assNum << ": " << assName;
	out << "\n**************************************************\n\n";
	out << right;

}

/*************************************************************************
 * AUTHOR       : Jack Armstrong & Austin Ayres
 * STUDENT ID   : 1017947        & 1012547
 * LAB #4       : Introduction to Arrays
 * CLASS        : CS1B
 * SECTION      : M/W/F 8:00am
 * DUE DATE     : 2/26
 ************************************************************************/

#include "Header.h"

int main()
{
	/*********************************************************************
	 * CONSTANTS
	 * -------------------------------------------------------------------
	 * PROGRAM CONSTANTS
	 * -------------------------------------------------------------------
	 * NUM_OF_NAMES	: The number of names to process
	 * -------------------------------------------------------------------
	 * CLASS HEADER CONSTANTS
	 * -------------------------------------------------------------------
	 * PROGRAMMER   : The name of the people who programmed the program
	 * CLASS   	    : The class we are currently in
	 * SECTION 	    : The time we have class
	 * ASS_NUM  	: The lab number
	 * ASS_NAME  	: The name of the lab
	 ********************************************************************/
	const int NUM_OF_NAMES = 10;

	const string PROGRAMMER = "Jack Armstrong & Austin Ayres";
	const string CLASS      = "CS1B";
	const string SECTION	= "F: 8:00am - 10:50am";
	const int    ASS_NUM    = 4;
	const string ASS_NAME   = "Introduction to Arrays";

	//STRINGS
	string namesAr		// IN & CALC & OUT - The array of names
	  	   [NUM_OF_NAMES];
	string searchItem;  // IN & CALC & OUT - The item to search for

	//INTEGERS
	int count;			// CALC            - The for loop counter
	int instances;		// CALC & OUT      - The amount of times a name pops up

	// Print the class header
	PrintHeader(cout, PROGRAMMER, CLASS, SECTION, ASS_NAME, ASS_NUM);

	// INPUT - Input the names into the array
	for(count = 0; count < NUM_OF_NAMES; count++)
	{
		cout << "Enter name #" << count+1 << ": ";
		getline(cin, namesAr[count]);
	} // END FOR

	cout << endl;

	// PROCESSING - Ask the user for the name they want to search for until
	//			    the user inputs "done"
	cout << "Who do you want to search for (enter done to exit)? ";
	getline(cin, searchItem);

	// Go until the user enters done
	while(searchItem != "done")
	{
		// Get the number of instances of searchItem
		instances = InstancesOfStringInAr(NUM_OF_NAMES, namesAr, searchItem);

		// Print the appropriate message
		if(instances > 1)
		{
			cout << "There are " << instances << " instances of the name " << searchItem << "." << endl;
		}
		else if(instances == 1)
		{
			cout << "There is one instance of the name " << searchItem << "." << endl;
		}
		else // instances == 0
		{
			cout << searchItem << "'s name does not exist in this list." << endl;
		} // END IF

		cout << endl;
		cout << "Who do you want to search for (enter done to exit)? ";
		getline(cin, searchItem);
	} //END WHILE
	cout << endl << "Thank you for using my program." << endl;


	return 0;
}




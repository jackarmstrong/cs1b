/**********************************************************
 * AUTHOR     : Jack Armstrong
 * STUDENT ID : 1017947
 * LAB #TEMPLATE: Template
 * CLASS      : CS1B
 * SECTION    : M/W 8:00am
 * DUE DATE   :
 **********************************************************/

#include <iostream>
#include <iomanip>
using namespace std;

// Documentation that goes here will be discussed later
int main() {
	/*******************************************************
	 * CONSTANTS
	 * ----------------------------------------------------
	 * USED FOR CLASS HEADING – ALL WILL BE OUTPUT
	 * ----------------------------------------------------
	 * PROGRAMMER : Programmer's Name
	 * CLASS : Student's Course
	 * SECTION : Class Days and Times
	 * LAB_NUM : Lab Number (specific to this lab)
	 * LAB_NAME : Title of the Lab
	 *****************************************************/

	const char PROGRAMMER[30] = "Jack Armstrong";
	const char CLASS[5] = "CS1B";
	const char SECTION[25] = "M/W 8:00am";
	const int LAB_NUM = 5;
	const char LAB_NAME[17] = "Template";
	// OUTPUT – Class Heading
	cout << left;
	cout << "**************************************************";
	cout << "\n* PROGRAMMED BY : " << PROGRAMMER;
	cout << "\n* " << setw(14) << "CLASS" << ": " << CLASS;
	cout << "\n* " << setw(14) << "SECTION" << ": " << SECTION;
	cout << "\n* LAB #" << setw(9) << LAB_NUM << ": " << LAB_NAME;
	cout << "\n**************************************************\n\n";
	cout << right;
	return 0;
}

/*************************************************************************
 * AUTHOR       : Jack Armstrong & Sina Heydarimolaei
 * STUDENT ID   : 1017947        & 1026012
 * LAB #3       : Functions - GCD
 * CLASS        : CS1B
 * SECTION      : M/W/F 8:00am
 * DUE DATE     : 2/19
 ************************************************************************/

#include "Lab3.h"

/*************************************************************************
 * FUNCTION GetHeader
 * -----------------------------------------------------------------------
 * This function returns the class header as a string
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 			- programmer : The programmer's name
 * 			- className  : The class name
 * 			- section    : The class section
 * 			- labName    : The assignment name
 * 			- labNum     : The assignment number
 * POST-CONDITIONS:
 * This function returns the specified class header as a string.
 ************************************************************************/
string GetHeader(string programmer // IN - The programmer's name
		       , string className  // IN - The class name
		       , string section    // IN - The class section
		       , string labName    // IN - The assignment name
		       , int    labNum)    // IN - The assignment type
{
	ostringstream out; // CALC - The ostringstream to output to
	// OUTPUT Class Heading
	out << left;
	out << "**************************************************";
	out << "\n* PROGRAMMED BY : " << programmer;
	out << "\n* " << setw(14) << "CLASS" << ": " << className;
	out << "\n* " << setw(14) << "SECTION" << ": " << section;
	out << "\n* Lab #" << setw(9) << labNum << ": " << labName;
	out << "\n**************************************************\n\n";
	out << right;

	return out.str();
}

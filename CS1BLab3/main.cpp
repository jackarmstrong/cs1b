/*************************************************************************
 * AUTHOR       : Jack Armstrong & Sina Heydarimolaei
 * STUDENT ID   : 1017947        & 1026012
 * LAB #3       : Functions - GCD
 * CLASS        : CS1B
 * SECTION      : M/W/F 8:00am
 * DUE DATE     : 2/19
 ************************************************************************/

#include "Lab3.h"

/*************************************************************************
 * This program will take in two numbers and print the GCD (Greatest
 * Common Divisor) to the file "oFile.txt".  It will repeat that 4 times.
 * -----------------------------------------------------------------------
 * INPUTS:
 * 		- num1 : The first number in the GCD calculation
 * 		- num2 : The second number in the GCD calculation
 * OUTPUTS:
 * 		- gcd  : The GCD of num1 & num2
 ************************************************************************/
int main()
{
	/*********************************************************************
	 * CONSTANTS
	 * -------------------------------------------------------------------
	 * USED FOR CLASS HEADING - ALL WILL BE OUTPUT
	 * -------------------------------------------------------------------
	 * PROGRAMMER 		: Programmer's Name
	 * CLASS 			: Student's Course
	 * SECTION 			: Class Days and Times
	 * LAB_NUM 			: Lab Number (specific to this lab)
	 * LAB_NAME		 	: Title of the Lab
	 * -------------------------------------------------------------------
	 * PROGRAM CONSTANTS
	 * -------------------------------------------------------------------
	 * OUTPUT_FILE_NAME	: The output file name
	 * TIMES_TO_PROMPT	: The # of times to calculate the GCD
	 ********************************************************************/

	const string PROGRAMMER     = "Jack Armstrong & Sina heydarimolaei";
	const string CLASS          = "CS1B";
	const string SECTION        = "M/W 8:00am";
	const int    LAB_NUM        = 3;
	const string LAB_NAME       = "Functions - GCD";

	const string OUTPUT_FILE_NAME = "oFile.txt";
	const int    TIMES_TO_PROMPT  = 4;

	int num1;		// IN & CALC	   : The first number for the GCD
	int num2;		// IN & CALC	   : The second number for the GCD
	int count;		//      CALC	   : The for loop counter
	int gcd;		//      CALC & OUT : The GCD of num1 & num2
	ofstream fout;	//             OUT : The file output stream

	// open the output file
	fout.open(OUTPUT_FILE_NAME.c_str());
	PrintHeader(cout, PROGRAMMER, CLASS, SECTION, LAB_NAME, LAB_NUM);
	PrintHeader(fout, PROGRAMMER, CLASS, SECTION, LAB_NAME, LAB_NUM);

	cout << GetHeader(PROGRAMMER, CLASS, SECTION, LAB_NAME, LAB_NUM);
	fout << GetHeader(PROGRAMMER, CLASS, SECTION, LAB_NAME, LAB_NUM);

	for(count = 0; count < TIMES_TO_PROMPT; count++)
	{
		// INPUT - Input num1 & num2 from the user
		cout << left;
		cout << setw(26) << "Enter the first integer:";
		cin  >> num1;

		cout << setw(26) << "Enter the second integer: ";
		cin  >> num2;
		cout << right;

		// PROCESSING - Calculate the GCD of num1 & num2
		gcd = GetGCD(num1, num2);

		// OUTPUT - Print the GCD for num1 & num2 to the output file
		cout << endl;
		fout << "The GCD for " << num1 << " & " << num2 << " = " << gcd
			 << endl;
		fout << endl;
	} // END FOR(count)

	// OUTPUT - Print the closing message
	cout << "Thank you for using my GCD calculator!\n";
	cout << setfill('-') << setw(50) << "-" << endl;

	fout.close();


	return 0;
}

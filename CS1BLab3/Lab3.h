/*************************************************************************
 * AUTHOR       : Jack Armstrong & Sina Heydarimolaei
 * STUDENT ID   : 1017947        & 1026012
 * LAB #3       : Functions - GCD
 * CLASS        : CS1B
 * SECTION      : M/W/F 8:00am
 * DUE DATE     : 2/19
 ************************************************************************/

#ifndef LAB3_H_
#define LAB3_H_

#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <sstream>
using namespace std;

/*************************************************************************
 * FUNCTION swap
 * -----------------------------------------------------------------------
 * This function returns nothing, but it swaps the two parameter's values,
 * so that if you call swap(a, b), a will be equal to b and b will be equal
 * to a.
 ************************************************************************/
void swap(int &num1		// IN & OUT - The first number
		, int &num2);	// IN & OUT - The second number

/*************************************************************************
 * FUNCTION GetGCD
 * -----------------------------------------------------------------------
 * This function will take in two numbers and return the greatest common
 * divisor of the two numbers.  If one of the two numbers equals zero,
 * the function returns the other number.
 ************************************************************************/
int GetGCD(int num1		// IN - The first number
		 , int num2);	// IN - The second number

/*************************************************************************
 * FUNCTION PrintHeader
 * -----------------------------------------------------------------------
 * This function prints the class header to the specified output stream.
 ************************************************************************/
void PrintHeader(ostream& out      // IN - The output stream to print to
		       , string programmer // IN - The programmer's name
		       , string className  // IN - The class name
		       , string section    // IN - The class section
		       , string assName    // IN - The assignment name
		       , int assNum);      // IN - The assignment type

/*************************************************************************
 * FUNCTION GetHeader
 * -----------------------------------------------------------------------
 * This function returns the class header as a string
 ************************************************************************/
string GetHeader(string programmer // IN - The programmer's name
		       , string className  // IN - The class name
		       , string section    // IN - The class section
		       , string labName    // IN - The assignment name
		       , int    labNum);   // IN - The assignment type

#endif /* LAB3_H_ */

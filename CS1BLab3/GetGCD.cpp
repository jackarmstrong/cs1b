/*************************************************************************
 * AUTHOR       : Jack Armstrong & Sina Heydarimolaei
 * STUDENT ID   : 1017947        & 1026012
 * LAB #3       : Functions - GCD
 * CLASS        : CS1B
 * SECTION      : M/W/F 8:00am
 * DUE DATE     : 2/19
 ************************************************************************/

#include "Lab3.h"

/*************************************************************************
 * FUNCTION GetGCD
 * -----------------------------------------------------------------------
 * This function will take in two numbers and return the greatest common
 * divisor of the two numbers.  If one of the two numbers equals zero,
 * the function returns the other number.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 				- num1 : The first number in the GCD calculation
 * 				- num2 : The second number in the GCD calculation
 * POST-CONDITIONS:
 * This function returns the GCD of num1 & num2.
 ************************************************************************/
int GetGCD(int num1		// IN - The first number
		 , int num2)	// IN - The second number
{

	int remainder; // CALC - The remainder
	int result;	   // CALC - The result of the GCD calculation

	if (num1 < num2)
	{
		// make num1 > num2
		swap(num1, num2);
	} // END IF
	if(num2 == 0)
	{
		result = num1;
		remainder = 0;
	} // END IF
	else
	{
		remainder = num1 % num2;
		if(remainder == 0)
		{
			result = num2;
		} // END IF
	} // END ELSE
	while(remainder != 0)
	{
		result = remainder;
		remainder = num1 % num2;
		num1 = remainder;

		// swap num1 & num2 so that num1 > num2
		swap(num1, num2);
	} // END WHILE

	return result;
}

/*************************************************************************
 * FUNCTION swap
 * -----------------------------------------------------------------------
 * This function returns nothing, but it swaps the two parameter's values,
 * so that if you call swap(a, b), a will be equal to b and b will be equal
 * to a.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 		- num1 : The first number to swap
 * 		- num2 : The second number to swap
 * POST-CONDITIONS:
 * 		- num1 : Will be equal to the starting value of num2
 * 		- num2 : Will be equal to the starting value of num1
 ************************************************************************/
void swap(int &num1		// IN & OUT - The first number
		, int &num2)	// IN & OUT - The second number
{
	int tempNum1;
	tempNum1 = num1;

	num1 = num2;
	num2 = tempNum1;
}


/*************************************************************************
 * AUTHOR		: Jack Armstrong & Jesse Nguyen
 * STUDENT ID	: 1017947        & 272637
 * LAB #5		: Binary Search
 * CLASS		: CS1B
 * SECTION		: MWF 8:00
 * DUE DATE		: 3/4
 ************************************************************************/

#ifndef HEADER_H_
#define HEADER_H_

#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

/*************************************************************************
 * FUNCTION BinarySearch
 * -----------------------------------------------------------------------
 * This function takes in a searchItem, integer array, and array size and
 * returns the index of searchItem in the integer array using the binary
 * search algorithm.
 ************************************************************************/
int BinarySearch(int searchItem		 // IN - The item to search for
		       , int intAr[]		 // IN - The integer array
			   , const int AR_SIZE); // IN - The size of the array

/*************************************************************************
 * FUNCTION SequentialSearch
 * -----------------------------------------------------------------------
 * This function takes in a searchItem, integer array, and array size and
 * returns the index of searchItem in the integer array using the sequential
 * search algorithm.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 		searchItem	: The item to search for in the array
 * 		intAr[]		: The integer array
 * 		AR_SIZE		: The size of the array
 * POST-CONDITIONS:
 * This function returns the index of searchItem in intAr[].
 ************************************************************************/
int SequentialSearch(int searchItem		 // IN - The item to search for
		           , int intAr[]		 // IN - The integer array
			       , const int AR_SIZE); // IN - The size of the array

/*************************************************************************
 * FUNCTION OutputArray
 * -----------------------------------------------------------------------
 * This function will output the array passed to it.
 ************************************************************************/
void OutputArray(const int AR[]			// IN - The array to print
			   , const int AR_SIZE);	// IN - The array size

/*************************************************************************
 * FUNCTION SequentialSearchWithUser
 * -----------------------------------------------------------------------
 * This function will take from the user four integer inputs and print the
 * index of them in the array to the user.
 ************************************************************************/
void SequentialSearchWithUser(int intAr[]		  // IN - The array
							, const int AR_SIZE); // IN - The array size

/*************************************************************************
 * FUNCTION BinarySearchWithUser
 * -----------------------------------------------------------------------
 * This function will prompt the user for an integer and tell them the index
 * of the integer in the array four times.
 ************************************************************************/
void BinarySearchWithUser(int intAr[]			// IN - The array
						, const int AR_SIZE);	// IN - The array size

/*************************************************************************
 * FUNCTION InsertionSort
 * -----------------------------------------------------------------------
 * This function takes in an array and sorts the array with the insertion
 * sort algorithm.
 ************************************************************************/
void InsertionSort(int intAr[]			// IN - The array to sort
				 , const int AR_SIZE);	// IN - The array size

/*************************************************************************
 * FUNCTION PrintHeader
 * -----------------------------------------------------------------------
 * This function will print the class header.
 ************************************************************************/
void PrintHeader(const string PROGRAMMER  // IN - The programmer's name
			   , const string CLASS		  // IN - The class
			   , const string SECTION	  // IN - The section
			   , const int    LAB_NUM	  // IN - The lab number
			   , const string LAB_NAME);  // IN - The lab name


#endif /* HEADER_H_ */

/*************************************************************************
 * AUTHOR		: Jack Armstrong & Jesse Nguyen
 * STUDENT ID	: 1017947        & 272637
 * LAB #5		: Binary Search
 * CLASS		: CS1B
 * SECTION		: MWF 8:00
 * DUE DATE		: 3/4
 ************************************************************************/

#include "Header.h"

/*************************************************************************
 * FUNCTION BinarySearchWithUser
 * -----------------------------------------------------------------------
 * This function will prompt the user for an integer and tell them the index
 * of the integer in the array four times.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 		- intAr[]	: The int array to work with
 * 		- AR_SIZE	: The size of the int array
 * POST-CONDITIONS:
 * This function will prompt the user for an integer and tell the the index
 * four times.
 ************************************************************************/
void BinarySearchWithUser(int intAr[]			// IN - The array
						, const int AR_SIZE)	// IN - The array size
{
	/*********************************************************************
	 * CONSTANTS
	 * -------------------------------------------------------------------
	 * TIMES	: The # of times to prompt the user for an integer
	 ********************************************************************/
	const int TIMES = 4;

	int searchItem;		// IN & CALC & OUT - The item to search for
	int index;			//      CALC & OUT - The index of the search item

	// Loop four times
	for(int i = 0; i < TIMES; i++)
	{
		// Prompt the user
		cout << "Enter an integer to search for: ";
		cin  >> searchItem;

		// Search the array
		index = BinarySearch(searchItem, intAr, AR_SIZE);

		// Check if it was found or not
		if(index == -1)
		{
			cout << searchItem << " was not found!";
		}
		else
		{
			cout << "The integer " << searchItem << " was found in index #" << index << ".";
		} // END IF
		cout << endl << endl;
	} // END FOR

	cout << endl;
}

/*************************************************************************
 * AUTHOR		: Jack Armstrong & Jesse Nguyen
 * STUDENT ID	: 1017947        & 272637
 * LAB #5		: Binary Search
 * CLASS		: CS1B
 * SECTION		: MWF 8:00
 * DUE DATE		: 3/4
 ************************************************************************/

#include "Header.h"

/*************************************************************************
 * FUNCTION SequentialSearchWithUser
 * -----------------------------------------------------------------------
 * This function will take from the user four integer inputs and print the
 * index of them in the array to the user.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 		intAr[]		: The integer array
 * 		AR_SIZE		: The size of the array
 * POST-CONDITIONS:
 * See description
 ************************************************************************/
void SequentialSearchWithUser(int intAr[]		  // IN - The array
							, const int AR_SIZE)  // IN - The array size
{
	/*********************************************************************
	 * CONSTANTS
	 * -------------------------------------------------------------------
	 * TIMES: # of times to prompt the user
	 ********************************************************************/
	const int TIMES = 4;

	int searchItem;		// IN & CALC & OUT - The item to search for
	int index;			//      CALC & OUT - The index of the searchItem

	// Go through the loop 4 times
	for(int i = 0; i < TIMES; i++)
	{
		// Prompt the user
		cout << "Enter an integer to search for: ";
		cin  >> searchItem;

		index = SequentialSearch(searchItem, intAr, AR_SIZE);

		if(index == -1)
		{
			cout << searchItem << " was not found!";
		}
		else
		{
			cout << "The integer " << searchItem << " was found in index #" << index << ".";
		} // END IF
		cout << endl << endl;
	} // END FOR
	cout << endl;
}

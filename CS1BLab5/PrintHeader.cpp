/*************************************************************************
 * AUTHOR		: Jack Armstrong & Jesse Nguyen
 * STUDENT ID	: 1017947        & 272637
 * LAB #5		: Binary Search
 * CLASS		: CS1B
 * SECTION		: MWF 8:00
 * DUE DATE		: 3/4
 ************************************************************************/

#include "Header.h"

/*************************************************************************
 * FUNCTION PrintHeader
 * -----------------------------------------------------------------------
 * This function will print the class header.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 		- PROGRAMMER	: The name of the programmer(s)
 * 		- CLASS			: The programmer's class
 * 		- SECTION		: The programmer's section
 * 		- LAB_NUM		: The programmer's lab's number
 * 		- LAB_NAME		: The programmer's lab's name
 * POST-CONDITIONS:
 * This function will print the class header to cout.
 ************************************************************************/
void PrintHeader(const string PROGRAMMER  // IN - The programmer's name
			   , const string CLASS		  // IN - The class
			   , const string SECTION	  // IN - The section
			   , const int    LAB_NUM	  // IN - The lab number
			   , const string LAB_NAME)	  // IN - The lab name
{
	// OUTPUT � Class Heading
	 cout << left;
	 cout << "**************************************************";
	 cout << "\n* PROGRAMMED BY : " << PROGRAMMER;
	 cout << "\n* " << setw(14) << "CLASS" << ": " << CLASS;
	 cout << "\n* " << setw(14) << "SECTION" << ": " << SECTION;
	 cout << "\n* LAB #" << setw(9) << LAB_NUM << ": " << LAB_NAME;
	 cout << "\n**************************************************\n\n";
	 cout << right;
}

/*************************************************************************
 * AUTHOR		: Jack Armstrong & Jesse Nguyen
 * STUDENT ID	: 1017947        & 272637
 * LAB #5		: Binary Search
 * CLASS		: CS1B
 * SECTION		: MWF 8:00
 * DUE DATE		: 3/4
 ************************************************************************/

#include "Header.h"

/*************************************************************************
 * FUNCTION BinarySearch
 * -----------------------------------------------------------------------
 * This function takes in a searchItem, integer array, and array size and
 * returns the index of searchItem in the integer array using the binary
 * search algorithm.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 		searchItem	: The item to search for in the array
 * 		intAr[]		: The integer array
 * 		AR_SIZE		: The size of the array
 * POST-CONDITIONS:
 * This function returns the index of searchItem in intAr[].
 ************************************************************************/
int BinarySearch(int searchItem		 // IN - The item to search for
		       , int intAr[]		 // IN - The integer array
			   , const int AR_SIZE)  // IN - The size of the array
{
	int low;	// CALC - The current low index
	int high;	// CALC - The current high index
	int mid;	// CALC - The current middle index

	// Initialize to starting values
	low = 0;
	high = AR_SIZE-1;

	// while we are in a range...
	while(low <= high)
	{
		// the middle is the average of low & high...
		mid = (low + high) / 2;

		// if the middle is the item...
		if(searchItem == intAr[mid])
		{
			// the mid is the index
			return mid;
		}
		else if(searchItem > intAr[mid])
		{
			// else if the searchItem is greater than the middle
			// the low is brought up
			low = mid + 1;
		}
		else if(searchItem < intAr[mid])
		{
			// else if the searchItem is smaller than the middle
			// the high is brought down
			high = mid - 1;
		} // END IF
	} // END WHILE
	// We never found it - the result is -1
	return -1;
}

/*************************************************************************
 * FUNCTION SequentialSearch
 * -----------------------------------------------------------------------
 * This function takes in a searchItem, integer array, and array size and
 * returns the index of searchItem in the integer array using the sequential
 * search algorithm.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 		searchItem	: The item to search for in the array
 * 		intAr[]		: The integer array
 * 		AR_SIZE		: The size of the array
 * POST-CONDITIONS:
 * This function returns the index of searchItem in intAr[].
 ************************************************************************/
int SequentialSearch(int searchItem		 // IN - The item to search for
		           , int intAr[]		 // IN - The integer array
			       , const int AR_SIZE)  // IN - The size of the array
{
	int index;		// CALC & OUT - The current index
	bool found;		// CALC		  - Whether searchItem is found

	// Initialize to starting values
	index = 0;
	found = false;

	// While not found and inside array
	while(!found && index < AR_SIZE)
	{
		// if the current element is the searchItem
		if(intAr[index] == searchItem)
		{
			// found is true
			found = true;
		}
		else
		{
			// otherwise go to next element
			index++;
		} // END IF
	} // END WHILE

	// if found return index otherwise -1
	return (found ? index : -1);
}

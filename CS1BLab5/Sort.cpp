/*************************************************************************
 * AUTHOR		: Jack Armstrong & Jesse Nguyen
 * STUDENT ID	: 1017947        & 272637
 * LAB #5		: Binary Search
 * CLASS		: CS1B
 * SECTION		: MWF 8:00
 * DUE DATE		: 3/4
 ************************************************************************/

#include "Header.h"

/*************************************************************************
 * FUNCTION InsertionSort
 * -----------------------------------------------------------------------
 * This function takes in an array and sorts the array with the insertion
 * sort algorithm.
 * -----------------------------------------------------------------------
 * PRE-CONDITINS:
 * 		intAr[]		: The unsorted array
 * 		AR_SIZE		: The size of the array
 * POST-CONDITIONS:
 * 		intAr[]		: The sorted array
 ************************************************************************/
void InsertionSort(int intAr[]			// IN - The array to sort
				 , const int AR_SIZE)	// IN - The array size
{
	int temp;	// CALC - A placeholder
	int j;		// CALC - Inner loop counter
	int i;		// CALC - Outer loop counter


	// Loop through every element
	for(i = 1; i < AR_SIZE; i++)
	{
		temp = intAr[i];
		j = i - 1;

		// Shift the elements over to make room for the current element
		while(j >= 0 && intAr[j] > temp)
		{
			intAr[j+1] = intAr[j];
			j--;
		} // END WHILE
		// put the current element in the new slot
		intAr[j+1] = temp;
	} // END FOR
}

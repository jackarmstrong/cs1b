/*************************************************************************
 * AUTHOR		: Jack Armstrong & Jesse Nguyen
 * STUDENT ID	: 1017947        & 272637
 * LAB #5		: Binary Search
 * CLASS		: CS1B
 * SECTION		: MWF 8:00
 * DUE DATE		: 3/4
 ************************************************************************/

#include "Header.h"

/*************************************************************************
 * FUNCTION OutputArray
 * -----------------------------------------------------------------------
 * This function will output the array passed to it.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 		AR[]    - The array to print
 * 		AR_SIZE - The size of the array to print
 * POST-CONDITIONS:
 * This function will output AR[] to the console.
 ************************************************************************/
void OutputArray(const int AR[]			// IN - The array to print
			   , const int AR_SIZE)		// IN - The array size
{
	// Print every element of the array
	for(int i = 0; i < AR_SIZE; i++)
	{
		cout << "Index #" << i << ": " << AR[i] << endl;
	}
	cout << endl;
}

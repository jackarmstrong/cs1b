/*************************************************************************
 * AUTHOR		: Jack Armstrong & Jesse Nguyen
 * STUDENT ID	: 1017947        & 272637
 * LAB #5		: Binary Search
 * CLASS		: CS1B
 * SECTION		: MWF 8:00
 * DUE DATE		: 3/4
 ************************************************************************/

#include "Header.h"

/*************************************************************************
 * This program will do the following in order:
 * - Print an array
 * - Ask the user for inputs and tell the user the index of the input 4
 * 	 times using sequential search
 * - Sort the array
 * - Print the sorted array
 * - Ask the user for inputs and tell the user the index of the input 4
 *   times using binary search.
 * -----------------------------------------------------------------------
 * OUTPUTS:
 * 		- intArray : The integer array to work with
 ************************************************************************/
int main()
{
	 /********************************************************************
	 * CONSTANTS
	 * -------------------------------------------------------------------
	 * USED FOR CLASS HEADING � ALL WILL BE OUTPUT
	 * -------------------------------------------------------------------
	 * PROGRAMMER 	: Programmer's Name
	 * CLASS 		: Student's Course
	 * SECTION 		: Class Days and Times
	 * LAB_NUM 		: Lab Number (specific to this lab)
	 * LAB_NAME 	: Title of the Lab
	 * -------------------------------------------------------------------
	 * AR_SIZE		: The size of the array
	 ********************************************************************/
	 const string PROGRAMMER = "Jack Armstrong & Jesse Nguyen";
	 const string CLASS      = "CS1B";
	 const string SECTION    = "MWF: 8:00";
	 const int LAB_NUM       = 5;
	 const string LAB_NAME   = "Binary Search";

	const int AR_SIZE = 8;

	int intArray[8]		// CALC & OUT - The array to work with
		= {4, 1, 7, 12, 8, 13, 9, 21};

	// Print the class header
	PrintHeader(PROGRAMMER, CLASS, SECTION, LAB_NUM, LAB_NAME);

	// Output the array
	OutputArray(intArray, AR_SIZE);

	// Ask the user for integers and output the index of each input 4 times
	// using sequential search
	SequentialSearchWithUser(intArray, AR_SIZE);

	// Insertion sort the array
	cout << "Performing Insertion Sort!\n\n";
	InsertionSort(intArray, AR_SIZE);

	// Output the sorted array
	OutputArray(intArray, AR_SIZE);

	// Ask the user for integers and output the index of each input 4 times
	// using binary search
	BinarySearchWithUser(intArray, AR_SIZE);
	return 0;
}

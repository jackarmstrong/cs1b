/**************************************************************************
 * AUTHOR        : Jack Armstrong & Cole Cooper
 * STUDENT ID    : 1017947        & 385328
 * LAB# 2        : Theme Park Day Planner
 * CLASS         : CS1B
 * SECTION       : MWF: 8am
 * DUE DATE      : 2/5/16
 *************************************************************************/

#include "Lab2.h"

/**************************************************************************
 * FUNCTION PrintHeader
 * ------------------------------------------------------------------------
 * This function receives an user name, assignment name, type, and number
 * then outputs the appropriate header.
 * 	- returns nothing
 * ------------------------------------------------------------------------
 * PRE-CONDITIONS
 * 		userName : Name(s) of user(s)
 * 		asName   : Assignment Name has to be previously defined
 * 		asType   : Assignment Type has to be previously defined
 * 		asNum    : Assignment Number has to be previously defined
 *
 * POST-CONDITIONS
 * 		This function will output the class heading.
 *
 *************************************************************************/
void PrintHeader(string userName,  //IN - user name(s)
				 string asName,	   //IN - assignment Name
				 char 	asType,    //IN - assignment type -
				 	 	 	 	   //     (LAB or assignment)
				 int  	asNum)	   //IN - assignment number
{

	cout << left;
	cout << "**********************************************************\n";
	cout << "*  PROGRAMMED BY : " << userName;
	cout << "\n*  " << setw(14) << "CLASS" << ": CS1B";
	cout << "\n*  " << setw(14) << "SECTION" << ": MWF - 8a-9:50a";
	cout << "\n*  " ;
	if (toupper(asType) == 'L')
	{
		cout << "LAB #" << setw(9);
	} // END IF
	else
	{
		cout << "ASSIGNMENT #" << setw(2);
	} // END ELSE
	cout << asNum << ": " << asName;
	cout << "\n**************************************************\n\n";
	cout << right;
}



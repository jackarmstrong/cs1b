/**************************************************************************
 * AUTHOR        : Jack Armstrong & Cole Cooper
 * STUDENT ID    : 1017947        & 385328
 * LAB# 2        : Theme Park Day Planner
 * CLASS         : CS1B
 * SECTION       : MWF: 8am
 * DUE DATE      : 2/5/16
 *************************************************************************/

#include "Lab2.h"

/*************************************************************************
 * Lab #2 - Functions - Coin Flip
 * -----------------------------------------------------------------------
 * This program simulates flipping a coin repeatedly and continues until
 * three consecutive heads are tossed.
 ************************************************************************/
int main() {
	/*********************************************************************
	 * CONSTANTS
	 * -------------------------------------------------------------------
	 * USED FOR CLASS HEADING ALL WILL BE OUTPUT
	 * -------------------------------------------------------------------
	 * PROGRAMMER : Programmer's Name
	 * CLASS : Student's Course
	 * SECTION : Class Days and Times
	 * LAB_NUM : Lab Number (specific to this lab)
	 * LAB_NAME : Title of the Lab
	 ********************************************************************/

	const string PROGRAMMER = "Jack Armstrong";
	const string CLASS      = "CS1B";
	const string SECTION    = "M/W 8:00am";
	const int    LAB_NUM    = 2;
	const string LAB_NAME   = "Functions - Coin Flip";

	string userName;	// IN &        OUT - The user's name
	char gender;        // IN & CALC & OUT - The user's gender
	int numTails;       //      CALC & OUT - The number of tails flipped
	int numHeads;       //      CALC & OUT - The number of heads flipped

	// OUTPUT Class Heading
	PrintHeader(PROGRAMMER, LAB_NAME, 'l', LAB_NUM);

	// seed random
	srand(time(NULL));

	cout << "Welcome to coin toss!  Get 3 heads in a row to win!\n\n";

	/*********************************************************************
	 * INPUT
	 * -------------------------------------------------------------------
	 * Inputs the user's name and gender
	 * 	- userName : The user's name
	 * 	- gender   : The user's gender
	 ********************************************************************/
	GetUserInfo(userName,gender);

	cout << "\nTry to get three heads in a row. Good luck "
		 <<(gender == 'M'? "Mr.":"Ms.") << userName << "!" << endl << endl;


	/*********************************************************************
	 * PROCESSING
	 * ------------------------------------------------------------------
	 * Flip coins until three heads in a row are gotten
	 ********************************************************************/
	TossCoins(numTails, numHeads);

	cout << endl;

	// OUTPUT - Output the average number of heads and total number of
	//  		coins flipped
	OutputResults(numTails, numHeads);



	return 0;
}





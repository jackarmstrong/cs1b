/**************************************************************************
 * AUTHOR        : Jack Armstrong & Cole Cooper
 * STUDENT ID    : 1017947        & 385328
 * LAB# 2        : Theme Park Day Planner
 * CLASS         : CS1B
 * SECTION       : MWF: 8am
 * DUE DATE      : 2/5/16
 *************************************************************************/

#include "Lab2.h"

/*************************************************************************
 * FUNCTION GetUserInfo
 * -----------------------------------------------------------------------
 * This function prompts the user for their name and gender, and returns
 * the user's name and uppercase gender ('M' or 'F')
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS: <no pre-conditions>
 * POST-CONDITIONS: This function returns the user's name and gender
 * through the 2 parameters.
 ************************************************************************/
void GetUserInfo(string &userName // OUT - The user's name
		       , char   &gender)  // OUT - The user's gender
{
	// INPUT - Input the user's name and gender
	cout << left;
	cout << setw(27) << "What is your name?";
	getline(cin, userName);

	cout << setw(27) << "What is your gender (m/f):";
	cin.get(gender);
	cin.ignore(1000, '\n');
	cout << right;

	// Turn the gender into uppercase
	gender = toupper(gender);
}




/**************************************************************************
 * AUTHOR        : Jack Armstrong & Cole Cooper
 * STUDENT ID    : 1017947        & 385328
 * LAB# 2        : Theme Park Day Planner
 * CLASS         : CS1B
 * SECTION       : MWF: 8am
 * DUE DATE      : 2/5/16
 *************************************************************************/

#ifndef LAB2_H_
#define LAB2_H_

#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

/*************************************************************************
 * Global Constants
 * -----------------------------------------------------------------------
 * HEADS : heads boolean constant
 * TAILS : Tails boolean constant
 *************************************************************************/
const bool HEADS = true;
const bool TAILS = false;

/**************************************************************************
 * FUNCTION PrintHeader
 * ------------------------------------------------------------------------
 * This function receives an user name, assignment name, type, and number
 * then outputs the appropriate header.
 * 	- returns nothing
 * ------------------------------------------------------------------------
 * PRE-CONDITIONS
 * 		userName : Name(s) of user(s)
 * 		asName   : Assignment Name has to be previously defined
 * 		asType   : Assignment Type has to be previously defined
 * 		asNum    : Assignment Number has to be previously defined
 *
 * POST-CONDITIONS
 * 		This function will output the class heading.
 *
 *************************************************************************/
void PrintHeader(string userName,  //IN - user name(s)
				 string asName,	   //IN - assignment Name
				 char 	asType,    //IN - assignment type -
				 	 	 	 	   //     (LAB or assignment)
				 int  	asNum);	   //IN - assignment number

/*************************************************************************
 * FUNCTION GetUserInfo
 * -----------------------------------------------------------------------
 * This function prompts the user for their name and gender, and returns
 * the user's name and uppercase gender ('M' or 'F')
 ************************************************************************/
void GetUserInfo(string &userName // OUT - The user's name
		       , char   &gender); // OUT - The user's gender

/*************************************************************************
 * FUNCTION FlipCoin
 * -----------------------------------------------------------------------
 * This function returns a random boolean, True or False for heads or tails
 ************************************************************************/
bool FlipCoin();

/*************************************************************************
 * FUNCTION TossCoins
 * -----------------------------------------------------------------------
 * This function returns a new value for numTails and numHeads.
 ************************************************************************/

void TossCoins(int &numTails,  // OUT - the number of tails tossed
			   int &numHeads); // OUT - the number of heads tossed
/*************************************************************************
 * FUNCTION GetAverageHeads
 * -----------------------------------------------------------------------
 * This function returns the average for heads vs total
 ************************************************************************/

float GetAverageHeads(int numTails,  // IN : The number of tails flipped
		              int numHeads); // IN : The number of heads flipped

/*************************************************************************
 * FUNCTION GetAverageHeads
 * -----------------------------------------------------------------------
 * This function returns the average for heads vs total
 ************************************************************************/

void OutputResults(int numTails   // IN - The number of tails flipped
				 , int numHeads); // IN - The number of heads flipped


#endif /* LAB2_H_ */

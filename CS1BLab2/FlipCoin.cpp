/**************************************************************************
 * AUTHOR        : Jack Armstrong & Cole Cooper
 * STUDENT ID    : 1017947        & 385328
 * LAB# 2        : Theme Park Day Planner
 * CLASS         : CS1B
 * SECTION       : MWF: 8am
 * DUE DATE      : 2/5/16
 *************************************************************************/

#include "Lab2.h"

/*************************************************************************
 * FUNCTION FlipCoin
 * -----------------------------------------------------------------------
 * This function returns a random boolean, True or False for heads or tails.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS: <no pre-conditions>
 * POST-CONDITIONS: This function returns a boolean
 ************************************************************************/

bool FlipCoin()
{
	int randNum = rand() % 2;
	bool flip;
	if(randNum == 0)
	{
		flip = HEADS;
	}
	else
	{
		flip = TAILS;
	}
	return flip;
}


/**************************************************************************
 * AUTHOR        : Jack Armstrong & Cole Cooper
 * STUDENT ID    : 1017947        & 385328
 * LAB# 2        : Theme Park Day Planner
 * CLASS         : CS1B
 * SECTION       : MWF: 8am
 * DUE DATE      : 2/5/16
 *************************************************************************/

#include "Lab2.h"

/*************************************************************************
 * FUNCTION GetAverageHeads
 * -----------------------------------------------------------------------
 * This function returns the average for heads vs total
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS: numTails and numHeads
 * POST-CONDITIONS: Returns a float that is the average
 ************************************************************************/

float GetAverageHeads(int numTails, // IN : The number of tails flipped
		              int numHeads) // IN : The number of heads flipped
{
	return (numHeads / float(numTails + numHeads)) * 100;
}

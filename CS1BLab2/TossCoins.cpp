/**************************************************************************
 * AUTHOR        : Jack Armstrong & Cole Cooper
 * STUDENT ID    : 1017947        & 385328
 * LAB# 2        : Theme Park Day Planner
 * CLASS         : CS1B
 * SECTION       : MWF: 8am
 * DUE DATE      : 2/5/16
 *************************************************************************/

#include "Lab2.h"

/*************************************************************************
 * FUNCTION TossCoins
 * -----------------------------------------------------------------------
 * This function returns a new value for numTails and numHeads.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS: < No preconditions >
 * POST-CONDITIONS: Returns the number of tails and heads flipped.
 ************************************************************************/

void TossCoins(int &numTails, // OUT - the number of tails tossed
			   int &numHeads) // OUT - the number of heads tossed
{
	int numHeadsInRow;
	bool coinResult;
	numTails = 0;
	numHeads = 0;

	numHeadsInRow = 0;

	while (numHeadsInRow < 3)
	{
		cout << "Press <enter> to flip";
		cin.ignore(1000,'\n');

		coinResult = FlipCoin();

		if(coinResult == HEADS)
		{
			numHeadsInRow++;
			numHeads++;
			cout << "HEAD\n";
		}
		else
		{
			numHeadsInRow = 0;
			numTails++;
			cout << "TAIL\n";
		}
	}

}



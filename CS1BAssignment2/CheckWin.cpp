/*************************************************************************
 * AUTHOR       	: Jack Armstrong
 * STUDENT ID   	: 1017947
 * ASSIGNMENT #2	: Multi-Dimensional Array - Tic Tac Toe
 * CLASS        	: CS1B
 * SECTION      	: M/W 8:00am
 * DUE DATE     	: 2/29
 ************************************************************************/

#include "Assignment2.h"

/*************************************************************************
 * FUNCTION CheckWin
 * -----------------------------------------------------------------------
 * This function takes in a character 2D array that is a tic tac toe board
 * and returns the winner.  If X has won, it returns the constant PLAYER_X.
 * If O has won, it returns the constant PLAYER_O.  If nobody has won,
 * it returns the constant EMPTY_SPACE.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 				- boardAr[][NUM_COLS]: The board to check
 * POST-CONDITIONS:
 * Returns PLAYER_X if X has won, PLAYER_O if O has won, and EMPTY_SPACE
 * if nobody has one.
 ************************************************************************/
char CheckWin(const char boardAr[][NUM_COLS]) // IN - The board to check
{
	char winner;		// CALC & OUT - The winner (as currently found)

	int row;			// CALC - The current row to check
	int col;			// CALC - The current column to check
	bool foundWin;		// CALC - Whether the program has found a win yet
	char players[2]		// CALC - The array of players - the functions
	             	 	//        checks X for a win first then O
	             = {PLAYER_X, PLAYER_O};
	int curPlayer;		// CALC - The player index (players[0], players[1])

	// Initialize to starting values
	curPlayer = 0;
	foundWin = false;

	// go until a win is found or there is no win
	while(curPlayer < 2 && !foundWin)
	{
		// CHECK ROWS
		row = 0;
		foundWin = false;
		while(row < NUM_ROWS && !foundWin)
		{
			foundWin = true;
			col = 0;
			while(col < NUM_COLS && foundWin)
			{
				foundWin = foundWin && boardAr[row][col]
				                                    == players[curPlayer];
				col++;
			} // END WHILE(col, foundWin)
			row++;
		} // END WHILE(row, foundWin)
		// if found a win...
		if(foundWin)
		{
			winner = players[curPlayer];
		} // END IF(foundWin)
		else
		{
			// otherwise check the columns
			// CHECK COLUMNS
			col = 0;
			foundWin = false;
			while(col < NUM_COLS && !foundWin)
			{
				foundWin = true;
				row = 0;
				while(row < NUM_ROWS && foundWin)
				{
					foundWin = foundWin && boardAr[row][col]
					                                 == players[curPlayer];
					row++;
				} // END WHILE(row, foundWin)
				col++;
			} // END WHILE(col, foundWin
			// if we found a win,
			if(foundWin)
			{
				winner = players[curPlayer];
			} // END IF(foundWin)
			else
			{
				/*********************************************************
				 * otherwise check the primary diagonal
				 * The primary diagonal is the slashes in the following
				 * board:
				 * :\--
				 * :-\-
				 * :--\
				 ********************************************************/
				// CHECK PRIMARY DIAGONAL
				row = 0;
				col = 0;
				foundWin = true;
				while(row < NUM_ROWS && col < NUM_COLS && foundWin)
				{
					foundWin = foundWin && boardAr[row][col]
					                                 == players[curPlayer];
					row++;
					col++;
				} // END WHILE(row, col, foundWin)
				// if we found a win,
				if(foundWin)
				{
					winner = players[curPlayer];
				} // END IF(foundWin)
				else
				{
					// CHECK SECONDARY DIAGONAL
					/*****************************************************
					 * otherwise check the secondary diagonal
					 * The secondary diagonal is the slashes in the
					 * following board:
					 * :--/
					 * :-/-
					 * :/--
					 ****************************************************/
					row = 0;
					col = NUM_COLS - 1;
					foundWin = true;
					while(row < NUM_ROWS && col > -1 && foundWin)
					{
						foundWin = foundWin && boardAr[row][col]
						                            == players[curPlayer];
						row++;
						col--;
					} // END WHILE(row, col, foundWin)
					if(foundWin)
					{
						winner = players[curPlayer];
					} // END IF(foundWin)
				} // END ELSE
			} // END ELSE
		} // END ELSE
		curPlayer++;
	} // END WHILE(curPlayer, foundWin)

	// if we still have not found a win, return the EMPTY_SPACE character
	if(!foundWin)
	{
		winner = EMPTY_SPACE;
	} // END IF(foundWin)
	return winner;

}



/*************************************************************************
 * AUTHOR       	: Jack Armstrong
 * STUDENT ID   	: 1017947
 * ASSIGNMENT #2	: Multi-Dimensional Array - Tic Tac Toe
 * CLASS        	: CS1B
 * SECTION      	: M/W 8:00am
 * DUE DATE     	: 2/29
 ************************************************************************/

#include "Assignment2.h"

/*************************************************************************
 * FUNCTION SwitchToken
 * -----------------------------------------------------------------------
 * This function takes a token and returns the opposite token.  If it gets
 * 'X', it returns 'O'. Otherwise, it returns 'X'.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 		- token : The token to switch
 * POST-CONDITIONS:
 * This function takes a token and returns the opposite token
 ************************************************************************/
char SwitchToken(char token)	// IN - The token to switch
{
	char switchedToken;		// CALC & OUT - The switched token

	// IF the token equals 'X', return 'O'.
	if(token == PLAYER_X)
	{
		switchedToken = PLAYER_O;
	}
	else
	{
		// OTHERWISE, the opposite will be 'X'.
		switchedToken = PLAYER_X;
	}
	// Return the switched token
	return switchedToken;
}



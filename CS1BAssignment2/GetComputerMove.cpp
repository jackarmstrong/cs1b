/*************************************************************************
 * AUTHOR       	: Jack Armstrong
 * STUDENT ID   	: 1017947
 * ASSIGNMENT #2	: Multi-Dimensional Array - Tic Tac Toe
 * CLASS        	: CS1B
 * SECTION      	: M/W 8:00am
 * DUE DATE     	: 2/29
 ************************************************************************/

#include "Assignment2.h"

/*************************************************************************
 * FUNCTION GetComputerMove
 * -----------------------------------------------------------------------
 * This function takes in a board and the other player's token, and
 * returns through the parameters &row and &col the position of the
 * computer's move.  It also puts the computer's token in the board passed
 * to it.
 * The process the computer follows is simple:
 * IF the center spot is available -
 * 		Take it.
 * ELSE IF The computer can win -
 * 		Win
 * ELSE IF The opponent can win -
 * 		Block
 * ELSE
 * 		Go in first available slot
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 					- &row				   : The computer's move's row
 * 					- &col				   : The computer's move's column
 * 					-  boardAr[][NUM_COLS] : The board to operate in
 * 					-  token			   : The computer's token
 * POST-CONDITIONS:
 * This function returns nothing, but &row will equal the computer's
 * move's row, and &col will equal the computer's column, and
 * boardAr[row][col] will equal the computer's token.
 ************************************************************************/
void GetComputerMove(int  &row				   //      OUT - The comp's row
				   , int  &col				   //      OUT - The comp's col
				   , char  boardAr[][NUM_COLS] // IN & OUT - The board
				   , char token)			   // IN       - The comp's token
{
	bool foundMove;		// CALC - Whether the computer has found a move yet
	char otherToken;	// CALC - The opponent's token

	// Assign starting values to variables
	otherToken = SwitchToken(token);
	foundMove = false;

	// IF THE COMPUTER CAN GO IN THE MIDDLE ([1][1]), GO THERE.
	// The middle is an advantageous place to go in Tic Tac Toe
	if(boardAr[1][1] == EMPTY_SPACE)
	{
		row = 2;
		col = 2;
		foundMove = true;
	}
	// IF THE COMPUTER CAN WIN IN A ROW, WIN!!
	if(!foundMove)
	{
		// CHECK ROWS FOR OPENING
		row = 0;
		while(row < NUM_ROWS && !foundMove)
		{
			if(boardAr[row][0] == token && boardAr[row][1] == token &&
			   boardAr[row][2] == EMPTY_SPACE)
			{
				col = 3;
				foundMove = true;
			}
			else if(boardAr[row][0] == token && boardAr[row][1]
			                   == EMPTY_SPACE && boardAr[row][2] == token)
			{
				col = 2;
				foundMove = true;
			}
			else if(boardAr[row][0] == EMPTY_SPACE && boardAr[row][1]
			                         == token && boardAr[row][2] == token)
			{
				col = 1;
				foundMove = true;
			}
			row++;
		}
	}
	// IF THE COMPUTER CAN WIN IN A COLUMN, WIN!!
	if(!foundMove)
	{
		// CHECK COLUMNS FOR OPENING
		col = 0;
		while(col < NUM_COLS && !foundMove)
		{
			if(boardAr[0][col] == token && boardAr[1][col] == token &&
					boardAr[2][col] == EMPTY_SPACE)
			{
				row = 3;
				foundMove = true;
			}
			else if(boardAr[0][col] == token && boardAr[1][col]
			                   == EMPTY_SPACE && boardAr[2][col] == token)
			{
				row = 2;
				foundMove = true;
			}
			else if(boardAr[0][col] == EMPTY_SPACE && boardAr[1][col] ==
										token && boardAr[2][col] == token)
			{
				row = 1;
				foundMove = true;
			}
			col++;
		}
	}
	// IF THE COMPUTER CAN WIN IN THE PRIMARY DIAGONAL, WIN!!
	if(!foundMove)
	{
		// CHECK THE PRIMARY DIAGONAL
		if(boardAr[0][0] == token && boardAr[1][1] == token &&
											boardAr[2][2] == EMPTY_SPACE)
		{
			row = 3;
			col = 3;
			foundMove = true;
		}
		else if(boardAr[0][0] == token && boardAr[1][1] == EMPTY_SPACE &&
				boardAr[2][2] == token)
		{
			row = 2;
			col = 2;
			foundMove = true;
		}
		else if(boardAr[0][0] == EMPTY_SPACE && boardAr[1][1] == token &&
				boardAr[2][2] == token)
		{
			row = 1;
			col = 1;
			foundMove = true;
		}
	}
	// IF THE COMPUTER CAN WIN IN THE SECONDARY DIAGONAL, WIN!!
	if(!foundMove)
	{
		// CHECK THE SECONDARY DIAGONAL ([0][2], [1][1], [2][0])
		if(boardAr[0][2] == token && boardAr[1][1] == token &&
				boardAr[2][0] == EMPTY_SPACE)
		{
			row = 3;
			col = 1;
			foundMove = true;
		}
		else if(boardAr[0][2] == token && boardAr[1][1] == EMPTY_SPACE &&
				boardAr[2][0] == token)
		{
			row = 2;
			col = 2;
			foundMove = true;
		}
		else if(boardAr[0][2] == EMPTY_SPACE && boardAr[1][1] == token &&
				boardAr[2][0] == token)
		{
			row = 1;
			col = 3;
			foundMove = true;
		}
	}
	// IF THE COMPUTER CANNOT MAKE A WINNING MOVE, SEE IF THE COMPUTER
	// CAN BLOCK A WINNING MOVE
	// If the other player can win in a row, block him
	if(!foundMove)
	{
		row = 0;
		while(row < NUM_ROWS && !foundMove)
		{
			if(boardAr[row][0] == otherToken && boardAr[row][1] ==
					otherToken && boardAr[row][2] == EMPTY_SPACE)
			{
				col = 3;
				foundMove = true;
			}
			else if(boardAr[row][0] == otherToken && boardAr[row][1] ==
					EMPTY_SPACE && boardAr[row][2] == otherToken)
			{
				col = 2;
				foundMove = true;
			}
			else if(boardAr[row][0] == EMPTY_SPACE && boardAr[row][1] ==
					otherToken && boardAr[row][2] == otherToken)
			{
				col = 1;
				foundMove = true;
			}
			row++;
		}
	}
	// CHECK COLUMNS
	// If the other player can win in a column, block him
	if(!foundMove)
	{
		col = 0;
		while(col < NUM_COLS && !foundMove)
		{
			if(boardAr[0][col] == otherToken && boardAr[1][col] ==
					otherToken && boardAr[2][col] == EMPTY_SPACE)
			{
				row = 3;
				foundMove = true;
			}
			else if(boardAr[0][col] == otherToken && boardAr[1][col] ==
					EMPTY_SPACE && boardAr[2][col] == otherToken)
			{
				row = 2;
				foundMove = true;
			}
			else if(boardAr[0][col] == EMPTY_SPACE && boardAr[1][col] ==
					otherToken && boardAr[2][col] == otherToken)
			{
				row = 1;
				foundMove = true;
			}
			col++;
		}
	}
	// CHECK PRIMARY DIAGONAL ([0][0], [1][1], [2][2])
	// If the other player can win in the primary diagonal, block him
	if(!foundMove)
	{
		if(boardAr[0][0] == otherToken && boardAr[1][1] == otherToken &&
				boardAr[2][2] == EMPTY_SPACE)
		{
			row = 3;
			col = 3;
			foundMove = true;
		}
		else if(boardAr[0][0] == otherToken && boardAr[1][1] == EMPTY_SPACE
			&& boardAr[2][2] == otherToken)
		{
			row = 2;
			col = 2;
			foundMove = true;
		}
		else if(boardAr[0][0] == EMPTY_SPACE && boardAr[1][1] == otherToken
				&& boardAr[2][2] == otherToken)
		{
			row = 1;
			col = 1;
			foundMove = true;
		}
	}
	// CHECK SECONDARY DIAGONAL ([0][2], [1][1], [2][0])
	// If the other player can win in the secondary diagonal, block him
	if(!foundMove)
	{
		if(boardAr[0][2] == otherToken && boardAr[1][1] == otherToken &&
				boardAr[2][0] == EMPTY_SPACE)
		{
			row = 3;
			col = 1;
			foundMove = true;
		}
		else if(boardAr[0][2] == otherToken && boardAr[1][1] == EMPTY_SPACE
				&& boardAr[2][0] == otherToken)
		{
			row = 2;
			col = 2;
			foundMove = true;
		}
		else if(boardAr[0][2] == EMPTY_SPACE && boardAr[1][1] == otherToken
				&& boardAr[2][0] == otherToken)
		{
			row = 3;
			col = 1;
			foundMove = true;
		}
	}



	// IF THE COMPUTER CANNOT MAKE A WINNING MOVE OR BLOCK A WINNING MOVE,
	// MOVE IN THE NEXT AVAILABLE SLOT
	if(!foundMove)
	{
		// FIND FIRST OPEN SLOT
		row = 0;
		foundMove = false;
		while(!foundMove && row < NUM_ROWS)
		{
			col = 0;
			while(!foundMove && col < NUM_COLS)
			{
				foundMove = (boardAr[row][col] == EMPTY_SPACE);
				col++;
			} // END WHILE
			row++;
		} // END WHILE
	}
	boardAr[row-1][col-1] = token;
}


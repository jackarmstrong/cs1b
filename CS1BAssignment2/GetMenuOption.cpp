/*************************************************************************
 * AUTHOR       	: Jack Armstrong
 * STUDENT ID   	: 1017947
 * ASSIGNMENT #2	: Multi-Dimensional Array - Tic Tac Toe
 * CLASS        	: CS1B
 * SECTION      	: M/W 8:00am
 * DUE DATE     	: 2/29
 ************************************************************************/

#include "Assignment2.h"

/*************************************************************************
 * FUNCTION GetMenuOption
 * -----------------------------------------------------------------------
 * This function takes in nothing, and returns the user's option of what
 * to do as an enumerated type.  It prompts with MENU_PROMPT, and returns
 * the result as a MenuOption.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:	<NO PRE-CONDITIONS>
 * POST-CONDITIONS:
 * This function returns the user's choice as a MenuOption.
 ************************************************************************/
MenuOption GetMenuOption()
{
	int menuOptionAsInt; // IN & CALC - The user's choice as an integer

	bool valid;			 //      CALC - Whether the user's choice is valid
	valid = false;
	do // while(!valid)
	{
		cout << MENU_PROMPT;
		cin  >> menuOptionAsInt;

		// Check if it is valid
		if(menuOptionAsInt > -1 && menuOptionAsInt < 4)
		{
			valid = true;
		} // END IF
		else
		{
			cout << "Invalid menu option. Enter a menu option inbetween "
				 << "0 and 3.\n";
		} // END ELSE
	} while(!valid);

	cin.ignore(1000, '\n');

	// Return the integer as a MenuOption
	return MenuOption(menuOptionAsInt);
}



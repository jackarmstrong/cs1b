/*************************************************************************
 * AUTHOR       	: Jack Armstrong
 * STUDENT ID   	: 1017947
 * ASSIGNMENT #2	: Multi-Dimensional Array - Tic Tac Toe
 * CLASS        	: CS1B
 * SECTION      	: M/W 8:00am
 * DUE DATE     	: 2/29
 ************************************************************************/

#include "Assignment2.h"

/*************************************************************************
 * FUNCTION PlayOnePlayerMode
 * -----------------------------------------------------------------------
 * This function takes in a board and the name of the player playing
 * against the computer.  It asks the user if they want to go first, and
 * the human player is X if they want to go first.  Otherwise the human
 * player is O.  When the computer moves, it tells the user where the
 * computer went.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 			- playerName : The name of the player playing against the
 * 						   computer
 * POST-CONDITIONS:
 * This function will let the user play against the computer.
 ************************************************************************/
void PlayOnePlayerMode(char   boardAr[][NUM_COLS] // IN - The board to use
                     , string playerName)		  // IN - The name of the
												  //      human player
{
	char doYouWantToGoFirst;	// IN&CALC  - Whether the user wants to go first
	string playerX;				// CALC&OUT - The name of the player playing X
	string playerO;				// CALC&OUT - The name of the player playing O
	char winner;				// CALC&OUT - The winner of the game
	char turn;					// CALC&OUT - The current turn
	int compRow;				// CALC&OUT - The computer's move's row
	int compCol;				// CALC&OUT - The computer's move's column
	int maxMoves;				// CALC     - The maximum # of moves
	int curMove;				// CALC     - The current move
	bool isUsersTurn;			// CALC     - Whether it is the user's turn


	// Initialize maxMoves
	maxMoves = NUM_ROWS * NUM_COLS;

	// Initialize the board
	InitBoard(boardAr);

	// Ask the user if they want to go first
	cout << "Do you want to go first? (Y/N): ";
	cin.get(doYouWantToGoFirst);
	cin.ignore(1000, '\n');

	// Make it uppercase
	doYouWantToGoFirst = toupper(doYouWantToGoFirst);

	// Switch the players so that the user goes first/last
	if(doYouWantToGoFirst == 'Y')
	{
		playerX = playerName;
		playerO = "Computer";
	} // END IF
	else
	{
		playerX = "Computer";
		playerO = playerName;
	} // END ELSE

	// Initialize to starting values

	// X always goes first
	turn = PLAYER_X;

	// Start on the zeroth move
	curMove = 0;

	// Set the winner to nobody
	winner = EMPTY_SPACE;

	// Check if we start with the user's turn
	isUsersTurn = doYouWantToGoFirst == 'Y';

	// Loop until there are no more moves or someone has won
	while(curMove < maxMoves && winner == EMPTY_SPACE)
	{
		// Display the board
		DisplayBoard(boardAr);

		// If the computer has moved, tell the user where the computer went
		if( (curMove > 0 && !(doYouWantToGoFirst == 'Y') ) || (curMove > 1))
		{
			cout << "The computer moved at [" << compRow << "][" << compCol
				 << "]\n";
		} // END IF

		// If it is the user's turn, prompt them for their move
		// Otherwise, ask the computer to make a move
		if(isUsersTurn)
		{
			GetAndCheckInp(boardAr, turn, playerX, playerO);
		} // END IF
		else
		{
			GetComputerMove(compRow, compCol, boardAr, turn);
		} // END ELSE

		// Increment the move
		curMove++;
		// Check if there is a win yet
		winner = CheckWin(boardAr);
		// Change the turn
		turn = SwitchToken(turn);
		// Change whether it is the user's turn
		isUsersTurn = !isUsersTurn;
	} // END WHILE
	// Print the winning board
	DisplayBoard(boardAr);

	// Print who won
	OutputWinner(winner, playerX, playerO);
}

/*************************************************************************
 * AUTHOR       	: Jack Armstrong
 * STUDENT ID   	: 1017947
 * ASSIGNMENT #2	: Multi-Dimensional Array - Tic Tac Toe
 * CLASS        	: CS1B
 * SECTION      	: M/W 8:00am
 * DUE DATE     	: 2/29
 ************************************************************************/

#include "Assignment2.h"

/*************************************************************************
 * FUNCTION OutputWinner
 * -----------------------------------------------------------------------
 * This function takes in a character of the winner, the name of the person
 * playing X, and the name of the person playing O.  It then outputs the
 * winner of the game, based on the char winner. If winner equals EMPTY_SPACE,
 * the function assumes a tie game and prints a random message.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 			- winner	: The winner ('X' if O won, 'O' if O won, ' ' if tie)
 * 			- playerX	: The name of the person playing X
 * 			- playerO	: The name of the person playing O
 * POST-CONDITIONS:
 * This function outputs the winner and a suitable message.
 ************************************************************************/
void OutputWinner(char   winner		// IN - The winner as a character
		        , string playerX	// IN - The name of the person playing X
		        , string playerO)	// IN - The name of the person playing O
{
	/*********************************************************************
	 * CONSTANTS
	 * -------------------------------------------------------------------
	 * NUM_OF_TIE_MESSAGES	: The number of tie messages
	 * TIE_MESSAGES[]		: The tie messages
	 ********************************************************************/
	// All these synonyms for "tie game" were found at
	// "http://www.thesaurus.com/browse/tie+game"
	const int NUM_OF_TIE_MESSAGES = 10;
	const string TIE_MESSAGES[NUM_OF_TIE_MESSAGES] =
	{
			"Blanket finish",
			"Draw",
			"Even money",
			"Mexican standoff",
			"Neck-and-neck race",
			"Photo finish",
			"Six of one and half a dozen of the other",
			"Standoff",
			"Tie",
			"Wash"
	};

	int randTieMessageInd; // CALC & OUT - The random tie message index

	if(winner == EMPTY_SPACE)
	{
		// tie game
		randTieMessageInd = rand() % NUM_OF_TIE_MESSAGES;
		cout << TIE_MESSAGES[randTieMessageInd] << "!\n";
	} // END IF
	else
	{
		cout << (winner == PLAYER_X ? playerX : playerO) << " won the game!\n";
	} // END ELSE
}

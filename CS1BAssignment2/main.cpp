/*************************************************************************
 * AUTHOR       	: Jack Armstrong
 * STUDENT ID   	: 1017947
 * ASSIGNMENT #2	: Multi-Dimensional Array - Tic Tac Toe
 * CLASS        	: CS1B
 * SECTION      	: M/W 8:00am
 * DUE DATE     	: 2/29
 ************************************************************************/

#include "Assignment2.h"

/*************************************************************************
 * This program will prompt the user if they want to exit, set player names,
 * play against the computer, or play in two player mode.  It repeats until
 * the user chooses EXIT.
 * -----------------------------------------------------------------------
 * INPUTS:
 * 			- playerX		: The name of the player playing X
 * 			- playerO		: The name of the player playing O
 * 			- menuOption	: The user's choice of what to do
 * OUTPUTS:
 * 			- playerX		: The name of the player playing X
 * 			- playerO		: The name of the player playing O
 * 			- boardAr[][]	: The board array of characters
 ************************************************************************/
int main()
{
	// Seed the random generator
	srand(time(NULL));
	// Clear the screen before starting
	system("clear");


	char boardAr[NUM_ROWS][NUM_COLS];	// CALC & OUT - The board array

	string playerX;		// IN & OUT - The player who is playing X's name
	string playerO;		// IN & OUT - The player who is playing O's name

	MenuOption menuOption;	// IN & CALC - The user's menu choice

	// Initialize the board
	InitBoard(boardAr);

	// Initialize the names of the players so that if you play without
	// initializing, there is something there.
	playerX = "Player X";
	playerO = "Player O";

	// Prompt the user
	menuOption = GetMenuOption();

	// Loop until the user says exit
	while(menuOption != EXIT)
	{
		// Clear the screen
		system("clear");

		// Figure out what the user wants to do
		switch(menuOption)
		{
		case SET_PLAYER_NAMES:
			// Set the player names
			GetPlayers(playerX, playerO, false);
			break;
		case TWO_PLAYER_MODE:
			// Play a game in two player mode
			PlayTwoPlayerMode(boardAr, playerX, playerO);
			break;
		case ONE_PLAYER_MODE:
			// Get the name of the player
			GetPlayers(playerX, playerO, true);

			// Play in one player mode against the computer
			PlayOnePlayerMode(boardAr, playerX);
			break;
		case EXIT:
			// This should never happen
			cout << "What happened?!!?!?\n";
			break;
		} // END SWITCH(menuOption)
		// Prompt the user
		menuOption = GetMenuOption();
	} // END WHILE(menuOption != EXIT)



	return 0;
}

################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../CheckWin.cpp \
../DisplayBoard.cpp \
../GetAndCheckInp.cpp \
../GetComputerMove.cpp \
../GetMenuOption.cpp \
../GetPlayers.cpp \
../InitBoard.cpp \
../OutputWinner.cpp \
../PlayOnePlayerMode.cpp \
../PlayTwoPlayerMode.cpp \
../SwitchToken.cpp \
../main.cpp 

OBJS += \
./CheckWin.o \
./DisplayBoard.o \
./GetAndCheckInp.o \
./GetComputerMove.o \
./GetMenuOption.o \
./GetPlayers.o \
./InitBoard.o \
./OutputWinner.o \
./PlayOnePlayerMode.o \
./PlayTwoPlayerMode.o \
./SwitchToken.o \
./main.o 

CPP_DEPS += \
./CheckWin.d \
./DisplayBoard.d \
./GetAndCheckInp.d \
./GetComputerMove.d \
./GetMenuOption.d \
./GetPlayers.d \
./InitBoard.d \
./OutputWinner.d \
./PlayOnePlayerMode.d \
./PlayTwoPlayerMode.d \
./SwitchToken.d \
./main.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



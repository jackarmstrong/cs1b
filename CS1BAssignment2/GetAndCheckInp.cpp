/*************************************************************************
 * AUTHOR       	: Jack Armstrong
 * STUDENT ID   	: 1017947
 * ASSIGNMENT #2	: Multi-Dimensional Array - Tic Tac Toe
 * CLASS        	: CS1B
 * SECTION      	: M/W 8:00am
 * DUE DATE     	: 2/29
 ************************************************************************/

#include "Assignment2.h"

/*************************************************************************
 * FUNCTION GetAndCheckInp
 * -----------------------------------------------------------------------
 * This function will take in the name of the person playing X, the name
 * of the person playing O, the token ('O'/'X') character of the current
 * player, and prompts and error-checks a move and then puts the move
 * inside boardAr[][].  It will only let the user input valid moves.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 					token	: The token of the current player ('X', 'O')
 * 					playerX	: The name of the player playing X
 * 					playerO : The name of the player playing O
 * POST-CONDITIONS:
 * This function returns nothing, but changes the board for the user's move.
 ************************************************************************/
void GetAndCheckInp(char   boardAr[][3]	// IN & OUT - The board to move
                  , char   token		// IN       - The current player
                  , string playerX		// IN		- The name of the X player
                  , string playerO)		// IN		- The name of the O player
{
	bool valid;			// CALC		  - Whether the user's move is valid
	string playerName;	// CALC & OUT - The name of the current player
	int row;			// IN & CALC  - The row that the user inputed
	int col;			// IN & CALC  - The column that the user inputed

	// Initalize to starting value
	valid = false;

	// Figure out the name of the player that is moving
	if(token == PLAYER_X)
	{
		playerName = playerX;
	} // END IF
	else
	{
		playerName = playerO;
	} // END ELSE

	do // while(!valid)
	{
		// Input the row and column
		cout << token << "\'s turn: " << "Enter your move, " << playerName
			 << "! ";
		cin  >> row >> col;

		// Subtract 1 from the row and column for indexing the board
		row--;
		col--;

		// Check if the row is out of range
		if(row >= NUM_ROWS || row < 0)
		{
			cout << "Invalid row " << row+1 << ". Please enter a row between 1 and 3.\n";
		} // END IF
		// Check if the column is out of range
		else if(col >= NUM_COLS || col < 0)
		{
			cout << "Invalid column " << col+1 << ". Please enter a column between 1 and 3.\n";
		} // END ELSE IF
		// Check if there is already a token where the user wants to move
		else if(boardAr[row][col] != EMPTY_SPACE)
		{
			cout << "Invalid move " << row << " " << col << ". That spot is already taken.\n";
		} // END ELSE IF
		else
		{
			valid = true;
		} // END ELSE
	} while(!valid);

	// Make the move
	boardAr[row][col] = token;
}



/*************************************************************************
 * AUTHOR       	: Jack Armstrong
 * STUDENT ID   	: 1017947
 * ASSIGNMENT #2	: Multi-Dimensional Array - Tic Tac Toe
 * CLASS        	: CS1B
 * SECTION      	: M/W 8:00am
 * DUE DATE     	: 2/29
 ************************************************************************/

#ifndef ASSIGNMENT2_H_
#define ASSIGNMENT2_H_

#include <cstdlib>
#include <time.h>
#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

/*************************************************************************
 * ENUMS
 * -----------------------------------------------------------------------
 * ENUM MenuOption : This enumeration represents menu options in the
 * 				     Tic-Tac-Toe application
 * 				- EXIT
 * 				- SET_PLAYER_NAMES	: This option lets you set the name's
 * 									  of the people that are playing X & O
 * 				- TWO_PLAYER_MODE	: This lets you play in 2-player mode
 * 				- ONE_PLAYER_MODE	: This lets you play against the computer
 ************************************************************************/
enum MenuOption
{
	EXIT,
	SET_PLAYER_NAMES,
	TWO_PLAYER_MODE,
	ONE_PLAYER_MODE
};
/*************************************************************************
 * GLOBAL CONSTANTS
 * -----------------------------------------------------------------------
 * PROGRAM CONSTANTS
 * -----------------------------------------------------------------------
 * MENU_PROMPT	: The prompt for the user to enter the choice (see ENUMS)
 * NUM_ROWS		: Number of rows (3)
 * NUM_COLS		: Number of columns (3)
 * PLAYER_X		: The character for the person playing X
 * PLAYER_O		: The character for the person playing O
 * EMPTY_SPACE	: The representation of an empty space on the game board
 ************************************************************************/
const string MENU_PROMPT = "Menu Items\n"
						   "0) Exit\n"
						   "1) Set Player Names\n"
						   "2) Play in two player mode\n"
						   "3) Play in one player mode\n"
						   "   (against the computer)\n"
						   "Enter your choice: ";

const int NUM_ROWS = 3;
const int NUM_COLS = 3;

const char PLAYER_X = 'X';
const char PLAYER_O = 'O';
const char EMPTY_SPACE = ' ';

/*************************************************************************
 * FUNCTION SwitchToken
 * -----------------------------------------------------------------------
 * This function takes a token and returns the opposite token.  If it gets
 * 'X', it returns 'O'. Otherwise, it returns 'X'.
 ************************************************************************/
char SwitchToken(char token);	// IN - The token to switch

/*************************************************************************
 * FUNCTION InitBoard
 * -----------------------------------------------------------------------
 * This function takes in a board, and initializes it to all EMPTY_SPACE.
 ************************************************************************/
void InitBoard(char boardAr[][NUM_COLS]); // OUT - The board to initialize

/*************************************************************************
 * FUNCTION GetMenuOption
 * -----------------------------------------------------------------------
 * This function takes in nothing, and returns the user's option of what
 * to do as an enumerated type.  It prompts with MENU_PROMPT, and returns
 * the result as a MenuOption.
 ************************************************************************/
MenuOption GetMenuOption();

/*************************************************************************
 * FUNCTION GetComputerMove
 * -----------------------------------------------------------------------
 * This function takes in a board and the other player's token, and
 * returns through the parameters &row and &col the position of the
 * computer's move.  It also puts the computer's token in the board passed
 * to it.
 * The process the computer follows is simple:
 * IF the center spot is available -
 * 		Take it.
 * ELSE IF The computer can win -
 * 		Win
 * ELSE IF The opponent can win -
 * 		Block
 * ELSE
 * 		Go in first available slot
 ************************************************************************/
void GetComputerMove(int  &row				   //      OUT - The comp's row
				   , int  &col				   //      OUT - The comp's col
				   , char  boardAr[][NUM_COLS] // IN & OUT - The board
				   , char token);			   // IN       - The comp's token

/*************************************************************************
 * FUNCTION GetAndCheckInp
 * -----------------------------------------------------------------------
 * This function will take in the name of the person playing X, the name
 * of the person playing O, the token ('O'/'X') character of the current
 * player, and prompts and error-checks a move and then puts the move
 * inside boardAr[][].  It will only let the user input valid moves.
 ************************************************************************/
void GetAndCheckInp(char   boardAr[][3]	// IN & OUT - The board to move
                  , char   token		// IN       - The current player
                  , string playerX		// IN		- The name of the X player
                  , string playerO);	// IN		- The name of the O player

/*************************************************************************
 * FUNCTION GetPlayers
 * -----------------------------------------------------------------------
 * This function takes in bool of whether this is against the computer,
 * and returns the name of the person playing X, and the person playing O.
 * If isAgainstComputer is true, It will prompt "Enter the player's name:"
 * and assign playerX to the input, and playerO to the string "Computer".
 * Otherwise if isAgainstComputer is false, It will prompt for the name of
 * the person playing X and the name of the person playing O.
 ************************************************************************/
void GetPlayers(string &playerX			    // OUT - The name of the X player
		      , string &playerO			    // OUT - The name of the O player
		      , bool    isAgainstComputer); // IN  - Whether playing against comp

/*************************************************************************
 * FUNCTION DisplayBoard
 * -----------------------------------------------------------------------
 * This function takes in a tic tac toe board and clears the screen,
 * then prints the board passed to the console in an orderly format.
 ************************************************************************/
void DisplayBoard(const char boardAr[][NUM_COLS]); // IN - The board to print

/*************************************************************************
 * FUNCTION CheckWin
 * -----------------------------------------------------------------------
 * This function takes in a character 2D array that is a tic tac toe board
 * and returns the winner.  If X has won, it returns the constant PLAYER_X.
 * If O has won, it returns the constant PLAYER_O.  If nobody has won,
 * it returns the constant EMPTY_SPACE.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 				- boardAr[][NUM_COLS]: The board to check
 * POST-CONDITIONS:
 * Returns PLAYER_X if X has won, PLAYER_O if O has won, and EMPTY_SPACE
 * if nobody has one.
 ************************************************************************/
char CheckWin(const char boardAr[][NUM_COLS]); // IN - The board to check

/*************************************************************************
 * FUNCTION OutputWinner
 * -----------------------------------------------------------------------
 * This function takes in a character of the winner, the name of the person
 * playing X, and the name of the person playing O.  It then outputs the
 * winner of the game, based on the char winner. If winner equals EMPTY_SPACE,
 * the function assumes a tie game and prints a random message.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 			- winner	: The winner ('X' if O won, 'O' if O won, ' ' if tie)
 * 			- playerX	: The name of the person playing X
 * 			- playerO	: The name of the person playing O
 * POST-CONDITIONS:
 * This function outputs the winner and a suitable message.
 ************************************************************************/
void OutputWinner(char   winner		// IN - The winner as a character
		        , string playerX	// IN - The name of the person playing X
		        , string playerO);	// IN - The name of the person playing O

/*************************************************************************
 * FUNCTION PlayOnePlayerMode
 * -----------------------------------------------------------------------
 * This function takes in a board and the name of the player playing
 * against the computer.  It asks the user if they want to go first, and
 * the human player is X if they want to go first.  Otherwise the human
 * player is O.  When the computer moves, it tells the user where the
 * computer went.
 ************************************************************************/
void PlayOnePlayerMode(char   boardAr[][NUM_COLS] // IN - The board to use
                     , string playerName);		  // IN - The name of the
												  //      human player

/*************************************************************************
 * FUNCTION PlayTwoPlayerMode
 * -----------------------------------------------------------------------
 * This function will take in a board to use, the name of the person
 * playing X, and the name of the person playing O.
 * It will allow the two players to play against each other.
 ************************************************************************/
void PlayTwoPlayerMode(char boardAr[][NUM_COLS] // IN - The board array
                     , string playerX			// IN - X player's name
                     , string playerO);			// IN - O player's name


#endif /* ASSIGNMENT2_H_ */

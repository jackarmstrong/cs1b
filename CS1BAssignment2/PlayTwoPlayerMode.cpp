/*************************************************************************
 * AUTHOR       	: Jack Armstrong
 * STUDENT ID   	: 1017947
 * ASSIGNMENT #2	: Multi-Dimensional Array - Tic Tac Toe
 * CLASS        	: CS1B
 * SECTION      	: M/W 8:00am
 * DUE DATE     	: 2/29
 ************************************************************************/

#include "Assignment2.h"

/*************************************************************************
 * FUNCTION PlayTwoPlayerMode
 * -----------------------------------------------------------------------
 * This function will take in a board to use, the name of the person
 * playing X, and the name of the person playing O.
 * It will allow the two players to play against each other.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 		- playerX : The name of the person playing X
 * 		- playerO : The name of the person playing O
 * POST-CONDITIONS:
 * This function will allow two users to play against each other.
 ************************************************************************/
void PlayTwoPlayerMode(char boardAr[][NUM_COLS] // IN - The board array
                     , string playerX			// IN - X player's name
                     , string playerO)			// IN - O player's name
{

	int maxMoves;	// CALC       - The maximum # of moves that may be made
	int curMove;	// CALC		  - The current move #
	char turn;		// CALC		  - The character of the current player
	char winner;	// CALC & OUT - The character of the winner

	// Initialize the maximum number of moves
	maxMoves = NUM_ROWS * NUM_COLS;

	// Initialize the board
	InitBoard(boardAr);

	// Initialize the current move to 0
	curMove = 0;
	// Initialize the winner to no body
	winner = EMPTY_SPACE;
	// X always goes first
	turn = PLAYER_X;
	// Play until someone wins or runs out of moves
	while(winner == EMPTY_SPACE && curMove < maxMoves)
	{
		// Print the board
		DisplayBoard(boardAr);

		// Prompt the current player for their move
		GetAndCheckInp(boardAr, turn, playerX, playerO);

		// Check if anyone has won
		winner = CheckWin(boardAr);

		// Other person's turn
		turn = SwitchToken(turn);
		// Increment the current move
		curMove++;
	} // END WHILE
	// Display winning/tie position
	DisplayBoard(boardAr);

	// Print the winner of the game
	OutputWinner(winner, playerX, playerO);
}

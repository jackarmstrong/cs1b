/*************************************************************************
 * AUTHOR       	: Jack Armstrong
 * STUDENT ID   	: 1017947
 * ASSIGNMENT #2	: Multi-Dimensional Array - Tic Tac Toe
 * CLASS        	: CS1B
 * SECTION      	: M/W 8:00am
 * DUE DATE     	: 2/29
 ************************************************************************/

#include "Assignment2.h"

/*************************************************************************
 * FUNCTION GetPlayers
 * -----------------------------------------------------------------------
 * This function takes in bool of whether this is against the computer,
 * and returns the name of the person playing X, and the person playing O.
 * If isAgainstComputer is true, It will prompt "Enter the player's name:"
 * and assign playerX to the input, and playerO to the string "Computer".
 * Otherwise if isAgainstComputer is false, It will prompt for the name of
 * the person playing X and the name of the person playing O.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 		-  isAgainstComputer : Whether we are playing against the computer
 * POST-CONDITIONS:
 * This function returns the name of the player playing X, and the name
 * of the player playing O. If isAgainstComputer is true, playerO will
 * always be "Computer".
 ************************************************************************/
void GetPlayers(string &playerX			   // OUT - The name of the X player
		      , string &playerO			   // OUT - The name of the O player
		      , bool    isAgainstComputer) // IN  - Whether playing against comp
{
	// Check if we are playing against the computer
	if(!isAgainstComputer)
	{
		// If we are not, prompt for the X player's name and the O player's
		// name
		cout << "Enter the name of the player who is playing X: ";
		getline(cin, playerX);

		cout << "Enter the name of the player who is playing O: ";
		getline(cin, playerO);
	} // END IF
	else
	{
		// If we are playing against the computer, prompt for the user's
		// name and assign the O player's name to "Computer"
		cout << "Enter the player's name: ";
		getline(cin, playerX);
		playerO = "Computer";
	} // END ELSE
}

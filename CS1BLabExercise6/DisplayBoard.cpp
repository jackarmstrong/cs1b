/*************************************************************************
 * AUTHOR       	: Jack Armstrong
 * STUDENT ID   	: 1017947
 * LAB EXERCISE #6	: Multi-Dimensional Array - Tic Tac Toe Suplement
 * CLASS        	: CS1B
 * SECTION      	: M/W 8:00am
 * DUE DATE     	: 3/4
 ************************************************************************/

#include "Header.h"

/*************************************************************************
 * FUNCTION DisplayBoard
 * -----------------------------------------------------------------------
 * This function takes in a tic tac toe board and clears the screen,
 * then prints the board passed to the console in an orderly format.
 * -----------------------------------------------------------------------
 * SAMPLE OUTPUT FOR EMPTY BOARD WITH X IN UPPER CORNERS:
 * -----------------------------------------------------------------------
 *          1       2       3
 *       [1][1] | [1][2] | [1][3]
 *              |        |
 * 1        X   |        |   X
 *              |        |
 *      --------------------------
 *       [2][1] | [2][2] | [2][3]
 *              |        |
 * 2            |        |
 *              |        |
 *      --------------------------
 *       [3][1] | [3][2] | [3][3]
 *              |        |
 * 3            |        |
 *              |        |
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 				- boardAr[][NUM_COLS] : The board to print to the console
 * POST-CONDITIONS:
 * This function returns nothing, but clears the screen and prints the
 * boardAr out to the console in the specified format.
 ************************************************************************/
void DisplayBoard(const char boardAr[][NUM_COLS]) // IN - The board to print
{
	// Clear the screen every time before displaying the board
	system("clear");

	int row; // CALC - The current row that is being displayed
	int col; // CALC - The current column that is being displayed

	// Print the headers
	cout << setw(10) << "1" << setw(8) << "2" << setw(9) << "3\n";

	// Print the body of the board
	for (row = 0; row < NUM_ROWS; row++)
	{
		cout << setw(7) << "[" << row + 1 << "][1] | " << "[" << row + 1;
		cout << "][2] | " << "[" << row + 1 << "][3]" << endl;
		cout << setw(14) << "|" << setw(9) << "|" << endl;

		for (col = 0; col < NUM_COLS; col++)
		{
			switch (col)
			{
			case 0:
				// Print the row headings
				cout << row + 1 << setw(9) << boardAr[row][col];
				cout << setw(4) << "|";
				break;
			case 1:
				// Print the middle of the board
				cout << setw(4) << boardAr[row][col];
				cout << setw(5) << "|";
				break;
			case 2:
				// Print the far right of the board
				cout << setw(4) << boardAr[row][col] << endl;
				break;
			default:
				// This should never happen
				cout << "ERROR!\n\n";
			} // END SWITCH(col)
		} // END FOR(col)
		cout << setw(14) << "|" << setw(10) << "|\n";

		// Print seperate this row from the next row
		if (row != 2)
		{
			cout << setw(32) << "--------------------------\n";
		} // END IF(row != 2)
	} // END FOR(row)
	cout << endl << endl;
}

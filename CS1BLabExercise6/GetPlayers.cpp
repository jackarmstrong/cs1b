/*************************************************************************
 * AUTHOR       	: Jack Armstrong
 * STUDENT ID   	: 1017947
 * LAB EXERCISE #6	: Multi-Dimensional Array - Tic Tac Toe Suplement
 * CLASS        	: CS1B
 * SECTION      	: M/W 8:00am
 * DUE DATE     	: 3/4
 ************************************************************************/

#include "Header.h"

/*************************************************************************
 * FUNCTION GetPlayers
 * -----------------------------------------------------------------------
 * This function asks the user to input the names of the players and
 * returns what the user entered.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS: <NO PRE-CONDITIONS>
 * POST-CONDITIONS:
 * 			- playerX : The name of the player playing X
 * 			- playerO : The name of the player playing O
 ************************************************************************/
void GetPlayers(string &playerX  // OUT - The name of the person playing X
			  , string &playerO) // OUT - The name of the person playing O
{
	cout << "Enter the name of the person playing X: ";
	getline(cin, playerX);

	cout << "Enter the name of the person playing O: ";
	getline(cin, playerO);
}

/*************************************************************************
 * AUTHOR       	: Jack Armstrong
 * STUDENT ID   	: 1017947
 * LAB EXERCISE #6	: Multi-Dimensional Array - Tic Tac Toe Suplement
 * CLASS        	: CS1B
 * SECTION      	: M/W 8:00am
 * DUE DATE     	: 3/4
 ************************************************************************/

#include "Header.h"

/*************************************************************************
 * FUNCTION InitBoard
 * -----------------------------------------------------------------------
 * This function takes in a board, and initializes it to all EMPTY_SPACE.
 * -----------------------------------------------------------------------
 * PRE-CONTITIONS:
 * 		- boardAr[][NUM_COLS] : The board to initialize to all EMPTY_SPACE
 * POST-CONDITIONS:
 * This function changes boardAr[][] to all EMPTY_SPACE.
 ************************************************************************/
void InitBoard(char boardAr[][NUM_COLS]) // OUT - The board to initialize
{
	int row; // CALC - The current row
	int col; // CALC - The current column

	// Loop through the rows
	for(row = 0; row < NUM_ROWS; row++)
	{
		// Loop through the columns
		for(col = 0; col < NUM_COLS; col++)
		{
			// Initialize the current space to EMPTY_SPACE
			boardAr[row][col] = EMPTY_SPACE;
		} // END FOR(col)
	} // END FOR(row)
}

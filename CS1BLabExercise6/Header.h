/*************************************************************************
 * AUTHOR       	: Jack Armstrong
 * STUDENT ID   	: 1017947
 * LAB EXERCISE #6	: Multi-Dimensional Array - Tic Tac Toe Suplement
 * CLASS        	: CS1B
 * SECTION      	: M/W 8:00am
 * DUE DATE     	: 3/4
 ************************************************************************/

#ifndef HEADER_H_
#define HEADER_H_

#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

/*************************************************************************
 * GLOBAL CONSTANTS
 * -----------------------------------------------------------------------
 * NUM_ROWS		: The number of rows for the game (3)
 * NUM_COLS		: The number of columns for the game (3)
 * EMPTY_SPACE	: The empty space character (' ')
 * PLAYER_X		: The X character ('X')
 * PLAYER_O		: The O character ('O')
 ************************************************************************/
const int NUM_ROWS = 3;
const int NUM_COLS = 3;

const char EMPTY_SPACE = ' ';
const char PLAYER_X    = 'X';
const char PLAYER_O    = 'O';
/*************************************************************************
 * FUNCTION InitBoard
 * -----------------------------------------------------------------------
 * This function takes in a board, and initializes it to all EMPTY_SPACE.
 ************************************************************************/
void InitBoard(char boardAr[][NUM_COLS]); // OUT - The board to initialize

/*************************************************************************
 * FUNCTION DisplayBoard
 * -----------------------------------------------------------------------
 * This function takes in a tic tac toe board and clears the screen,
 * then prints the board passed to the console in an orderly format.
 * -----------------------------------------------------------------------
 * SAMPLE OUTPUT FOR EMPTY BOARD WITH X IN UPPER CORNERS:
 * -----------------------------------------------------------------------
 *          1       2       3
 *       [1][1] | [1][2] | [1][3]
 *              |        |
 * 1        X   |        |   X
 *              |        |
 *      --------------------------
 *       [2][1] | [2][2] | [2][3]
 *              |        |
 * 2            |        |
 *              |        |
 *      --------------------------
 *       [3][1] | [3][2] | [3][3]
 *              |        |
 * 3            |        |
 *              |        |
 ************************************************************************/
void DisplayBoard(const char boardAr[][NUM_COLS]); // IN - The board to print

/*************************************************************************
 * FUNCTION GetPlayers
 * -----------------------------------------------------------------------
 * This function asks the user to input the names of the players and
 * returns what the user entered.
 * -----------------------------------------------------------------------
 * PRE-CONDITIONS: <NO PRE-CONDITIONS>
 * POST-CONDITIONS:
 * 			- playerX : The name of the player playing X
 * 			- playerO : The name of the player playing O
 ************************************************************************/
void GetPlayers(string &playerX   // OUT - The name of the person playing X
			  , string &playerO); // OUT - The name of the person playing O

/*************************************************************************
 * FUNCTION GetAndCheckInp
 * -----------------------------------------------------------------------
 * This function will take in the name of the person playing X, the name
 * of the person playing O, the token ('O'/'X') character of the current
 * player, and prompts and error-checks a move and then puts the move
 * inside boardAr[][].  It will only let the user input valid moves.
 ************************************************************************/
void GetAndCheckInp(char   boardAr[]	// IN & OUT - The board to move
                                   [NUM_COLS]
                  , char   token		// IN       - The current player
                  , string playerX		// IN		- The name of the X player
                  , string playerO);	// IN		- The name of the O player
#endif /* HEADER_H_ */

/*************************************************************************
 * AUTHOR       	: Jack Armstrong
 * STUDENT ID   	: 1017947
 * LAB EXERCISE #6	: Multi-Dimensional Array - Tic Tac Toe Suplement
 * CLASS        	: CS1B
 * SECTION      	: M/W 8:00am
 * DUE DATE     	: 3/4
 ************************************************************************/

#include "Header.h"

/*************************************************************************
 * This program does the following:
 * - Ask the user for the name of the person playing X and O
 * - Outputs the name of the people playing X and O
 * - Displays the empty board
 * - Asks the X player to make a move
 * - Displays the board with the move made
 * -----------------------------------------------------------------------
 * INPUTS:
 * 			- playerX	: The name of the person playing X
 * 			- playerO	: The name of the person playing O
 * OUTPUTS:
 * 			- boardAr	: The board array
 ************************************************************************/
int main()
{
	char boardAr			// CALC & OUT - The board array
	[NUM_ROWS][NUM_COLS];
	string playerX;			// IN & OUT	  - The name of the person playing X
	string playerO;			// IN & OUT	  - The name of the person playing O

	// Initialize the board
	InitBoard(boardAr);

	// Ask the user for the names of the players
	GetPlayers(playerX, playerO);

	// Print the names of the players
	cout << "playerX: " << playerX << endl;
	cout << "playerO: " << playerO << endl;
	cout << endl;

	// Print the empty board
	cout << "EMPTY BOARD:\n";
	DisplayBoard(boardAr);

	// Prompt the user for their move
	GetAndCheckInp(boardAr, PLAYER_X, playerX, playerO);

	// Print the board
	cout << "BOARD THAT HAS BEEN MOVED:\n";
	DisplayBoard(boardAr);
	return 0;
}

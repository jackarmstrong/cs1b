/*
 * FindAvgOfIntAr.cpp
 *
 *  Created on: Feb 1, 2016
 *      Author: jackarmstrong
 */

float FindAvgOfIntAr(const int AR_SIZE, int ar[])
{
	int sum;
	int count;

	sum = 0;

	for(count = 0; count < AR_SIZE; count++)
	{
		sum += ar[count];
	}
	return float(sum) / AR_SIZE;
}



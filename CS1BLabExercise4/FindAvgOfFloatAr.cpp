/*
 * FindAvgOfFloatAr.cpp
 *
 *  Created on: Feb 1, 2016
 *      Author: jackarmstrong
 */

float FindAvgOfFloatAr(const int AR_SIZE, float ar[])
{
	float sum;
	int count;

	sum = 0;

	for(count = 0; count < AR_SIZE; count++)
	{
		sum += ar[count];
	}
	return float(sum) / AR_SIZE;
}



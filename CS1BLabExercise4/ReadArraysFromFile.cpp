/*
 * ReadArraysFromFile.cpp
 *
 *  Created on: Feb 1, 2016
 *      Author: jackarmstrong
 */
#include "LabExercise.h"

void ReadArraysFromFile(string inFileName, int intAr[], float floatAr[]
                      , const int AR_SIZE)
{
	ifstream fin;
	int count;


	fin.open(inFileName.c_str());

	count = 0;
	while(count < AR_SIZE && fin)
	{
		fin >> intAr[count];
		fin >> floatAr[count];
		count++;
	}
	fin.close();
}



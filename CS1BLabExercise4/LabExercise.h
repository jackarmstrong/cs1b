/*
 * LabExercise.h
 *
 *  Created on: Feb 1, 2016
 *      Author: jackarmstrong
 */

#ifndef LABEXERCISE_H_
#define LABEXERCISE_H_

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
using namespace std;

float FindAvgOfIntAr(const int AR_SIZE, int ar[]);
float FindAvgOfFloatAr(const int AR_SIZE, float ar[]);
void ReadArraysFromFile(string inFileName, int intAr[], float floatAr[]
                      , const int AR_SIZE);

#endif /* LABEXERCISE_H_ */

/*
 * main.cpp
 *
 *  Created on: Feb 1, 2016
 *      Author: jackarmstrong
 */
#include "LabExercise.h"

int main()
{
	const int AR_SIZE = 3;

	int intAr[AR_SIZE];
	float floatAr[AR_SIZE];

	string inputFileName;
	int inputFileNumber;
	float avg;
	int count;

	cout << "Enter the input file number (1, 2, 3, 4, or 5) (0 to exit): ";
	cin  >> inputFileNumber;
	cout << endl;

	while(inputFileNumber >= 1 && inputFileNumber <= 5)
	{

		inputFileName = "inputFile" + to_string(inputFileNumber) + ".txt";

		ReadArraysFromFile(inputFileName, intAr, floatAr, AR_SIZE);


		cout << setprecision(2) << fixed;

		avg = FindAvgOfIntAr(AR_SIZE, intAr);
		cout << "The average of the integers is: " << avg << endl;
		avg = FindAvgOfFloatAr(AR_SIZE, floatAr);
		cout << "The average of the floating-point #s is: " << avg << endl;

		cout << "The array's contents are:\n";
		cout << left << setw(10) << "INDEX" << left << setw(14)
			 << "INTEGER #" << right << "FLOATING POINT #" << endl;

		for(count = 0; count < AR_SIZE; count++)
		{
			cout << right << setw(3) << count+1 << setw(7) << " "
				 << setw(9) << intAr[count] << right << setw(21)
				 << floatAr[count] << endl;
		}
		cout << endl;
		cout << "Enter the input file number (1, 2, 3, 4, or 5) (0 to exit): ";
		cin  >> inputFileNumber;
		cout << endl;
	}
	return 0;
}



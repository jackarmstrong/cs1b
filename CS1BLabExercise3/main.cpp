
#include <iostream>
#include <string>
using namespace std;

int main()
{
	const int NUM_OF_CATAGORIES = 4;

	int ranges[NUM_OF_CATAGORIES]        = {        2,             12,              20,              40};
	float prices[NUM_OF_CATAGORIES]      = {     5.00,           8.50,           12.50,           18.00};
	string rangeNames[NUM_OF_CATAGORIES] = {"under 2", "between 3-12", "between 13-20", "between 21-40"};

	int numOfPackages[NUM_OF_CATAGORIES] = {        0,              0,               0,               0};

	float totalPrice;
	int userInputWeight;
	int packageCategory;
	int count;

	totalPrice = 0;

	cin >> userInputWeight;
	while(userInputWeight != 0)
	{
		packageCategory = 0;
		if(userInputWeight <= ranges[0])
		{
			packageCategory = 0;
		}
		else if(userInputWeight <= ranges[1])
		{
			packageCategory = 1;
		}
		else if(userInputWeight <= ranges[2])
		{
			packageCategory = 2;
		}
		else if(userInputWeight <= ranges[3])
		{
			packageCategory = 3;
		}
		numOfPackages[packageCategory]++;
		totalPrice+=prices[packageCategory];

		cin >> userInputWeight;
	} // END WHILE

	cout << endl;

	for(count = 0; count < NUM_OF_CATAGORIES; count++)
	{
		cout << numOfPackages[count] << " package" << (numOfPackages[count] == 1 ? "" : "s") << " " << rangeNames[count] << " lbs.\n";
	}
	cout << setprecision(2) << fixed;
	cout << "The total shipping price is $" << totalPrice << endl;

	return 0;
}
